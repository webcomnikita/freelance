<?php
return [
	'project_file_store_path' => '/projects/images/',
	'profile_file_store_path' => '/profiles/images/',
	'profile_portfolio_store_path' => '/profiles/portfolios/',
	'inquiry_file_store_path' => '/profiles/inquiries/',
	'message_file_store_path' => '/profiles/messages/',
	'bidding_file_store_path' => '/bidding/files/',

	'sms_head' 	 		=> 'AGCLGB',
	'sms_username'  	=> 'online',
	'sms_password' 		=> 'confirm',
	'sms_url' 			=> 'http://trans.instaclicksms.in/sendsms.jsp',
];
