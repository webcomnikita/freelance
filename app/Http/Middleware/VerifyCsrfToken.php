<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */


    //add an array of Routes to skip CSRF check
    protected $except = [
        'user/message/order-now-success', 
        'user/message/order-now-fail',
        'projects/buy-premium-success',
        'projects/buy-premium-fail'
    ];

}
