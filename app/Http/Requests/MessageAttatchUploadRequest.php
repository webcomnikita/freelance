<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageAttatchUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            //'project_id'    => 'required|exists:projects,id',
        ];
        $photos = count($this->input('files'));


        foreach(range(0, $photos) as $index) {
            $rules['files.' . $index] = 'mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls|max:10000';
        }
 
        return $rules;
    }
}
