<?php

namespace App\Http\Controllers\User\Activity;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helper;
use DB, Validator, Redirect, Auth, Crypt, Storage, Session;

use App\Models\Project\Order\Order;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $seller_order = Order::with('buyer')->where('seller_id',$user_id)->get();
        $buyer_order = Order::with('seller')->where('buyer_id',$user_id)->get();
        return view('user.activity.index',compact('seller_order','buyer_order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order_id = $request->input('order_id');
        $exptd_date = $request->expected_delivery_date;
        $ordr = Order::where('id',$order_id)->find($order_id);

        // $odr = new Order();
        $ordr->expected_delivery_date  = $exptd_date;
        $ordr->order_current_status    = 'order_accepted';
        $ordr->save();

        Session::flash('success','You have successfully accepted project order');
        return redirect()->route('user.my-activity');
    }

    public function deliverOrderBySeller(Request $request)
    {
        $order_id = $request->input('order_id');
        $order = Order::with('userProfile')->where('id', $order_id)->find($order_id);

        $order->seller_order_confirm        = 1;
        $order->seller_order_confirm_date   = date('Y-m-d H:i:s');
        $order->save();

        Session::flash('success','You have successfully delivered project order');
        return redirect()->route('user.my-activity');

    }

     public function deliverOrderByBuyer(Request $request)
     {
        $order_id = $request->input('order_buy_id');
        $order = Order::with('userProfile')->where('id', $order_id)->find($order_id);

        $order->buyer_order_confirm         = 1;
        $order->buyer_order_confirm_date    = date('Y-m-d H:i:s');

        if ($order->seller_order_confirm == 1) {
            $order->project_delivered   = 1;
        }

        $order->save();

        Session::flash('success','You have successfully received delivered project order');
        return redirect()->route('user.my-activity');

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seller = Order::with('buyer')->where('id',$id)->first();
        return view('user.activity.seller-single',compact('seller'));
    }

    public function buyerShow($id)
    {
        $buyer = Order::with('seller')->where('id',$id)->first();
        return view('user.activity.buyer-single',compact('buyer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
