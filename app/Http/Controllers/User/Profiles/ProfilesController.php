<?php

namespace App\Http\Controllers\User\Profiles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Helper;
use DB, Validator, Redirect, Auth, Crypt, Storage;

use App\Models\Profile\UserProfile, App\Models\Profile\ProfileImage, App\Models\Profile\ProfileLocation , App\Models\Profile\UserProfilePortfolio;



use App\Http\Requests\ProfileImageUploadRequest;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = UserProfile::with('user')
                                ->with('category')
                                // ->with('profile_locations')
                                // ->with('profile_images')
                                ->get();

        return view('user.profiles.index', compact('profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Helper::getAllCategories($list = true);
        return view('user.profiles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileImageUploadRequest $request)
    {
        //dd($request->all());
        $message = [];
        DB::beginTransaction();
        /* Insert data to profiles table */
        try {
            // Validate, then create if valid
            $data = $request->except('files', '_token');
            $slug = Helper::getUniqueSlug(new UserProfile(), $request->profile_title);
            $data['slug']       = $slug;
            $data['user_id']    = Auth::id();

            $validator = Validator::make($data, UserProfile::$rules);

            if ($validator->fails()) return  Redirect::back()->withErrors($validator)->withInput();
            
            $profile = UserProfile::create( $data );
        }catch(ValidationException $e)
        {
            return Redirect::back();
        }
        try {
            //loop through the items entered

            $files = $request->file('files');

            if(!empty($files)):
                foreach ($files as $photo) {
                    $image_data = [];
                    $filename       = strtolower(md5(uniqid().time())).'.'.$photo->getClientOriginalExtension();

                    $destinationPath = config('globals.profile_file_store_path');
                    Storage::disk('uploads')->put($destinationPath . $filename ,file_get_contents($photo));

                    $image_data['user_profile_id']  = $profile->id;
                    $image_data['image_path']       = $filename;

                    ProfileImage::create($image_data);
                }
            endif;
            // Validate, then create if valid
        } catch(ValidationException $e)
        {
            // Back to form
            return Redirect::back();
        }

        try {
            //loop through the locations entered

            $localities = $request->localities;

            if($localities):
                $longitudes = $request->longitudes;
                $latitudes  = $request->latitudes;
                foreach ($localities as $k => $locality) {
                    $locality_data = [];

                    $locality_data['user_profile_id']  = $profile->id;

                    $locality_data['latitude']      = $latitudes[$k];
                    $locality_data['longitude']     = $longitudes[$k];
                    $locality_data['name']          = $locality;

                    ProfileLocation::create($locality_data);
                }
            endif;
            // Validate, then create if valid
        } catch(ValidationException $e)
        {
            // Back to form
            return Redirect::back();
        }


        //portfolio 

        try {
            
           $pflprtflio_title = $request->title;
           $pflprtflio_description = $request->description;
           $pflprtflio_web_link = $request->web_link;
           $pflprtflio_image_path = $request->file('image_path');

           foreach ($pflprtflio_title as $key => $title) {

           $pflprtflio_title_slug = str_slug($pflprtflio_title[$key], '-');
          

             $mFile = $pflprtflio_image_path[$key];
             $filenames = strtolower(md5(uniqid().time())).'.'.$mFile->getClientOriginalExtension();
             $destinationPaths = config('globals.profile_portfolio_store_path');
             Storage::disk('uploads')->put($destinationPaths . $filenames ,file_get_contents($mFile));

             $pfolio_data = [];  
             $pfolio_data['user_profile_id']= $profile->id;
             $pfolio_data['title']   = $pflprtflio_title[$key];
             $pfolio_data['slug']    = $pflprtflio_title_slug;
             $pfolio_data['description'] = $pflprtflio_description[$key];
             $pfolio_data['web_link']   = $pflprtflio_web_link[$key];
             $pfolio_data['image_path'] = $filenames;


             $portfolio = UserProfilePortfolio::create( $pfolio_data );
           }

        }catch(ValidationException $e)
        {
            return Redirect::back();
        }
        // end portfolio



        // Commit the queries!
        DB::commit();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
      
        $sprofiles = UserProfile::with('user')
                                ->with('category')
                                ->where('slug', $slug)->first();

        $lc = UserProfile::where('slug', $slug)->first()->id;
        $location = ProfileLocation::where('user_profile_id', $lc)->take(1)->get();

        $img = ProfileImage::where('user_profile_id', $lc)->take(1)->get();
        $image = ProfileImage::where('user_profile_id', $lc)->get();

        return view('user.profiles.single', compact('sprofiles','location','img','image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request) {
        $take = 10;
        $skip = 0;

        if($request->take) {
            $take = $request->take;
        }

        if($request->skip) {
            $skip = $request->skip;
        }

        $search = UserProfile::with('user', 'category', 'profile_images', 'profile_locations')->take($take)->skip($skip)->get();

        return view('user.profiles.search',compact('search'));
    }

    public function myProfile()
    {
        
        return view('user.profiles.my-profile');
    }
}
