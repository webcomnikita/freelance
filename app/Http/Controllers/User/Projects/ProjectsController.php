<?php

namespace App\Http\Controllers\User\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Helper,Session, Storage;
use DB, Validator, Redirect, Auth, Crypt;
use App\Models\Project\Project, App\Models\Project\ProjectImage , App\Models\Project\ProjectLocation;
use App\Models\Project\Category, App\Models\Profile\UserProfile;
use App\User;
use App\Http\Requests\MessageAttatchUploadRequest, App\Http\Requests\InquiryFileUploadRequest;
use App\Models\Message\Message, App\Models\Message\MessageAttachement;
use App\Models\User\Biddings\Bidding, App\Models\User\Biddings\BiddingAttatchment;

use App\Models\User\Premium\Premiumuser;
use App\Models\Coupon\Coupon;
use App\Http\Requests\ProjectImageUploadRequest;
class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $projects = Project::with('projectimage')
                             ->with('projectlocation')
                             ->with('category')
                             ->where('user_id', $user_id)->get();

        

        
        return view('user.projects.index',compact('projects'));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        // $a = User::where('status', 1)->first();

        // if ($a) {
            $project_budgets = Project::$project_budgets;
            $categories = Helper::getAllCategories($list = true);
            return view('user.projects.create', compact('categories', 'project_budgets'));
        // }else{
        //     Session::flash('error', 'You have to approved by admin for posting a job');
        //     return redirect()->back();
        // }

        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectImageUploadRequest $request)
    {
        
        $message = '';
        DB::beginTransaction();
        /* Insert data to projects table */
        try {
            // Validate, then create if valid
            $data = $request->except('files', '_token');
            $slug = Helper::getUniqueSlug(new Project(), $request->name);
            $data['slug']       = $slug;
            $data['user_id']    = Auth::user()->id;

            //dd($data['user_id']);die();

            $validator = Validator::make($data, Project::$rules);
            
            if ($validator->fails()) return  Redirect::back()->withErrors($validator)->withInput();

            $project = Project::create( $data );
        }catch(ValidationException $e)
        {
            return Redirect::back();
        }
        try {
            //loop through the items entered

            $files = $request->file('files');

            if(!empty($files)):
                foreach ($files as $photo) {
                    $image_data = [];
                    $filename = $photo->store(config('globals.project_file_store_path'));

                    $image_data['project_id'] = $project->id;
                    $image_data['image_path'] = $filename;

                    ProjectImage::create($image_data);
                }
            endif;
            // Validate, then create if valid
        } catch(ValidationException $e)
        {
            // Back to form
            return Redirect::back();
        }

        try {
            //loop through the locations entered

            $localities = $request->localities;

            if($localities):
                $longitudes = $request->longitudes;
                $latitudes  = $request->latitudes;
                foreach ($localities as $k => $locality) {
                    $locality_data = [];

                    $locality_data['project_id']  = $project->id;

                    $locality_data['latitude']      = $latitudes[$k];
                    $locality_data['longitude']     = $longitudes[$k];
                    $locality_data['name']          = $locality;

                    ProjectLocation::create($locality_data);
                }
            endif;
            // Validate, then create if valid
        } catch(ValidationException $e)
        {
            // Back to form
            return Redirect::back();
        }

        // Commit the queries!
        DB::commit();

        $category = DB::table('categories')->where('id', $request->category_id)->first()->slug;
        return Redirect::route('dashboard', [$category, $slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

  
    public function show($category_name, $slug)
    {
        //$project_data = Helper::projectSlugGhost($slug);
        $coupon = Coupon::where('coupon_disable', 1)->get();
        $project_data = Project::with('category')->where('slug', $slug)->first();

        $user_id = Auth::user()->id;
        $project_user_id = Project::where('slug', $slug)->first()->user_id;

        $user_status = UserProfile::where('user_id',$user_id)->first()->status;
        $time = strtotime($project_data->created_at);
        $time_hr = date('Y-m-d H:i:s',$time);
        $time_diff = DB::table('disable_times')->select('time_in_hr')->max('time_in_hr');
        // dd($time_diff);die();
        $final = date('Y-m-d H:i:s', strtotime("+".$time_diff." hours", $time));

        $p_id = Project::where('slug', $slug)->first()->id;
        $bidding = Bidding::where('sender_id',$user_id)->where('project_id',$p_id)->first();
        $haveBidding =false ;
        if($bidding) {
           $haveBidding =true ;  
        }

      /*  $bidding_count = Bidding::where('sender_id',$user_id)->where('status', 0)->count('receiver_id');*/
        $bidding_count =0;
        $today=date('Y-m-d');

        $premiumchack = Premiumuser::where('buy_date', '<=', $today)
                           ->where('premium_end_date', '>=', $today)
                           ->where('status', 1)
                           ->orderBy('created_at', 'desc')->first();

        $lastPremiumPurchase = Premiumuser::orderBy('created_at', 'desc')->first();
        $lastPremium="";
        $havePurchase=false;

        if( count($lastPremiumPurchase)>0 ) {
             $lastPremium=$lastPremiumPurchase->premium_end_date;
             $havePurchase=true;
        }

        $isPremium=false;
        if( count($premiumchack)>0 ) {
            $isPremium=true;
        } else{

           if($havePurchase){
               // $lastPremium  $today
            $length = strtotime($today)-strtotime($lastPremium);
            $datediff= round($length / (60 * 60 * 24));
           // dd( $datediff );
            $minDifDays=$datediff%30;
           

            $mytime = strtotime($today );
            $before_last_day = date('Y-m-d H:i:s', strtotime("-".$minDifDays." days", $mytime)); 
            //dd( $before_last_day );
            $bidding_count = Bidding::where('sender_id',$user_id)
                                ->where('created_at', '>=', $before_last_day)->count('receiver_id');
 
           }else{

                 $mytime = strtotime($today );
                 $before_thirty_day = date('Y-m-d H:i:s', strtotime("-30 days", $mytime)); 
                 $bidding_count = Bidding::where('sender_id',$user_id)
                                ->where('created_at', '>=', $before_thirty_day)->count('receiver_id');
              }    

        }

        // $after_one_day = date('Y-m-d H:i:s', strtotime("+1 day", $time)); 
        // $before_one_day = date('Y-m-d H:i:s', strtotime("-1 day", $time)); 
                      
        //dd( $bidding_count );
        //dd($isPremium );
      //  die();
       

        if($project_data) {
            $project_images = ProjectImage::where('project_id', $project_data->id)->get();  

            return view('user.projects.view', compact('project_data', 'project_images','time_hr','final','user_id','project_user_id','user_status','haveBidding','bidding_count','bidding','isPremium','coupon','p_id'));
        }
    }



    public function changeDisableProject(Request $request)
    {
       $p_id = $request->input('project_hidden_id');

       $project_id = Project::find($p_id);
       $project_id->disable = 1;
       $project_id->save();

       Session::flash('success','You have successfully change disable status');
       return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showCategoryFilter($slug)
    {
        $user_id = Auth::user()->id;
        $categories = Category::where('slug',$slug)->first()->id;
        $projects = Project::where('category_id',$categories)->get();
        return view('user.projects.category-filter',compact('categories','user_id','projects'));
    }

    public function contactMeForProjectDetails($slug)
    {
        $project_user_id = Project::where('slug',$slug)->first()->user_id;
        $user_name = User::where('id',$project_user_id)->first()->name;
        $project_name = Project::where('slug',$slug)->first()->name;
        $project_price_range = Project::where('slug',$slug)->first()->budget;

        $project_id = Project::where('slug',$slug)->first()->id;
        //dd($project_slug);die();

        return view('user.projects.contact-me',compact('project_user_id','user_name','project_name','project_price_range','project_id'));
    }

    public function contactMeProjectSend(MessageAttatchUploadRequest $request)
    {
      
      $msg = new Message();
      $rcv = $request->input('receiver_id');
      $today1  = date("Y-m-d");
     
     
      $msg->receiver_id = $rcv;
      $msg->sender_id  = Auth::user()->id;
      $msg->message  = $request->input('message');
      $msg->subject  = 'Bidding price by freelancer';
      $msg->bidding_amount  = $request->input('bidding_price');
      $msg->message_date  = $today1;
     
      $msg->save();

        $files = $request->file('files');
        $my_mesage_id = $msg->id;
     
           if(!empty($files)){
               $arrImageData = [];
               foreach ($files as $photo) { 

                  $filename = strtolower(md5(uniqid().time())).'.'.$photo->getClientOriginalExtension();
                 
                    $destinationPath = config('globals.message_file_store_path');
                    Storage::disk('uploads')->put($destinationPath . $filename ,file_get_contents($photo));

                    array_push($arrImageData,$filename);

                }
                    foreach ($arrImageData as $ImageFileName){
                         $msg_attch1 = new MessageAttachement();
                         $msg_attch1->message_id= $my_mesage_id;
                         $msg_attch1->file_path= $ImageFileName;
                         $msg_attch1->save();
                     }
             }


    // ------------------------------------

      $bid = new Bidding();
      $bid_rcv = $msg->receiver_id;
      $today_bid  = date("Y-m-d");
     
     
      $bid->receiver_id = $bid_rcv;
      $bid->sender_id  = Auth::user()->id;
      $bid->message  = $msg->message;
      $bid->subject  = 'Bidding price by freelancer';
      $bid->bidding_amount  = $msg->bidding_amount;
      $bid->message_date  = $today_bid;
      $bid->project_id  = $request->input('project_id');
     
      $bid->save();

        $files1 = $request->file('files');
        $my_bidding_id = $bid->id;
     
           if(!empty($files1)){
               $arrImageData1 = [];
               foreach ($files1 as $photos) { 

                  $filename1 = strtolower(md5(uniqid().time())).'.'.$photos->getClientOriginalExtension();
                 
                    $destinationPath1 = config('globals.bidding_file_store_path');
                    Storage::disk('uploads')->put($destinationPath1 . $filename1 ,file_get_contents($photos));

                    array_push($arrImageData1,$filename1);

                }
                    foreach ($arrImageData1 as $ImageFileName1){
                         $bid_attatch = new BiddingAttatchment();
                         $bid_attatch->bidding_id= $my_bidding_id;
                         $bid_attatch->file_path= $ImageFileName1;
                         $bid_attatch->save();
                     }
             }
        
      
        Session::flash('success', 'You have successfully send mail for project');
        return redirect()->route('user.message.my-inbox');

   }

   public function browseJob()

   {

    $user_id = Auth::user()->id;

     // $user_profile_category_id = UserProfile::with('category')->where('user_id',$user_id)->where('status',1)->select('category_id')->get();

    $category = Category::all();
  
     
    // $projects = Project::with('projectimage')
    //                          ->with('projectlocation')
    //                          ->with('category')
    //                          ->whereIn('category_id',$user_profile_category_id)
    //                          ->get(); 
    $projects = Project::with('projectimage')
                             ->with('projectlocation')
                             ->with('category')
                             ->get(); 

    //dd($projects);die();

    return view('user.projects.browse',compact('projects','category'));
   }

   public function browseJobFilter(Request $request)
   {
      $category_id = $request->input('category_id');

      $project_category_id = Project::with('category')->where('category_id', $category_id)->get();

      //dd($project_category_id);die();

      return view('user.projects.browse-filter',compact('project_category_id'));
      
   }

   public function buyPremium()
   {

      $a = Auth::user()->id;

      // dd($a);die();

      $priceVal = '1000';
      $MERCHANT_KEY = 'eFPOJqhW';
      $SALT = 'Q8kM5RTT5L';
      $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

      $email = 'abc@test.test' ;
      $name = 'nikita' ;
      $mobile_no = '8767654343';
      $location = 'guwahati';
      $zip_code = '789876';

      return view('user.projects.buy-premium', compact('MERCHANT_KEY','txnid','hash_string','priceVal','name','email','mobile_no','location','zip_code','a'));
   }

   public function buyPremiumCoupon(Request $request)
   {

      $a = Auth::user()->id;

      $priceVal = $request->input('priceVal');
      $MERCHANT_KEY = 'eFPOJqhW';
      $SALT = 'Q8kM5RTT5L';
      $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

      $email = $request->input('email');
      $name = $request->input('name');
      $mobile_no = $request->input('phone');
      $location = $request->input('address1');
      $zip_code = $request->input('zipcode');
      $coupon = $request->input('coupon');

      $discountVal=$request->input('udf4');
      $TotalVal=$request->input('udf2');
      if( $TotalVal==null){
            $TotalVal=$priceVal;
      }
     
     /// dd($TotalVal);die();

      $mycoupon="";
      if( $discountVal==null){
          $discountVal=0;
      }else{
         $mycoupon=$coupon;

         Session::flash('error','You have already applied a coupon');
      }

      if( $discountVal==0 && $coupon !='0'){
          if($this->isValidCoupon($coupon)){
             $mycoupon=$coupon;
             $discountVal=$this->getCouponValue($coupon);

             $priceVal=$TotalVal - $discountVal;
              Session::flash('success','Coupon successfully applied');
          }else{

            Session::flash('error','Invalid coupon or coupon may be expired');
          }

     }
    
      $udf1=$a ;  //  seller profile Id
      $udf2=$TotalVal;
      $udf3=$mycoupon;
      $udf4=$discountVal;
      

     
      $productinfo  = 'Buy premium';

      $hash = "";

      $hash_string = $MERCHANT_KEY."|".$txnid."|".$priceVal."|".$productinfo."|".$name."|".$email."|".$udf1."|".$udf2."|".$udf3."|".$udf4."|||||||".$SALT;

      $hash = strtolower(hash('sha512', $hash_string));



      
    return view('user.projects.buy-premium-coupon', compact('MERCHANT_KEY','hash','txnid','hash_string','priceVal','name','email','mobile_no','location','zip_code','productinfo','a','udf1','udf2','discountVal','TotalVal','mycoupon','udf3','udf4'));
   }

   public function buyPremiumConfirm(Request $request)
    {

     $a = Auth::user()->id;

      $priceVal = $request->input('priceVal');
      $MERCHANT_KEY = 'eFPOJqhW';
      $SALT = 'Q8kM5RTT5L';
      $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

      $email = $request->input('email');
      $name = $request->input('name');
      $mobile_no = $request->input('phone');
      $location = $request->input('address');
      $zip_code = $request->input('zipcode');
      $coupon = $request->input('coupon');

      $discountVal=$request->input('udf4');
      $TotalVal=$request->input('udf2');

      $coupon=$request->input('coupon');
      $udf1=$request->input('udf1'); //  seller profile Id
      $udf2=$TotalVal;
      $udf3=$coupon;
      $udf4=$discountVal;

      $productinfo  = 'Buy premium';

      $hash = "";

      $hash_string = $MERCHANT_KEY."|".$txnid."|".$priceVal."|".$productinfo."|".$name."|".$email."|".$udf1."|".$udf2."|".$udf3."|".$udf4."|||||||".$SALT;

      $hash = strtolower(hash('sha512', $hash_string));

      

      return view('user.projects.buy-premium-confirm',compact('a','priceVal','MERCHANT_KEY','SALT','txnid','email','name','mobile_no','location','zip_code','coupon','discountVal','TotalVal','productinfo','hash','hash_string','coupon','udf1','udf2','udf3','udf4'));
    }

   public function getCouponValue($inputCoupon)
   {
       $CouponValue=0;
        $today=date('Y-m-d');
        $Couponchack = Coupon::where('coupon_start_date', '<=', $today)
                           ->where('coupon_end_date', '>=', $today)
                           ->where('coupon_disable', 1)
                           ->where('coupon_name', $inputCoupon)
                           ->orderBy('created_at', 'desc')->first();
        if( count($Couponchack)>0 ) {
            $CouponValue=$Couponchack->coupon_discount_amount;
        }                  



      return $CouponValue;
   }

   public function isValidCoupon($inputCoupon)
   {
        $validCoupon=false;
        $today=date('Y-m-d');
        $Couponchack = Coupon::where('coupon_start_date', '<=', $today)
                           ->where('coupon_end_date', '>=', $today)
                           ->where('coupon_disable', 1)
                           ->where('coupon_name', $inputCoupon)
                           ->orderBy('created_at', 'desc')->first();
        if( count($Couponchack)>0 ) {
            $validCoupon=true;
        }                  


      return $validCoupon;
   }

  
    public function paymentZeroPaid(Request $request)
   {
     $premiumusers = new Premiumuser();
     $a = Auth::user()->id;
     $buy_dt = date("Y-m-d");
     $buy_date = strtotime( $buy_dt );
     $after_day = date('Y-m-d H:i:s', strtotime("+1 month", $buy_date));
    

     $premiumusers->user_id = $a;
     $premiumusers->package_amount = $request->input('amount');
     $premiumusers->buy_date = $buy_dt;
     $premiumusers->premium_end_date = $after_day;
     $txnid_id = DB::table('premiumusers')->max('txnid');
        if($txnid_id==0){
            $txnid_id=1000;
        }else{
            $txnid_id= $txnid_id+1; 
        }

     $premiumusers->txnid = $txnid_id;
     $premiumusers->status = 1;

     $coupon_name = $request->input('coupon_id');
     $coupon_id = Coupon::where('coupon_name',$coupon_name)->first();

        if (isset($coupon_id->id)) {
            $coupon_id_name = $coupon_id->id;
        }else
            $coupon_id_name = "";
     
   
     $premiumusers->coupon_name = $coupon_name;
     $premiumusers->coupon_id = $coupon_id_name;
     $premiumusers->coupon_applied = 1;
     $premiumusers->save();

     Session::flash('success','Your have paid successfully for premium user');
     return redirect()->route('dashboard');


   }

   public function buyPremiumInit(Request $request)
   {
      $user_id = $request->input('user_id');
      $package_amount = $request->input('package_amount');
      $buy_date = date("Y-m-d");

      $premium = new Premiumuser();
      $premium->user_id = $user_id;
      $premium->package_amount = $package_amount;
      $premium->buy_date = $buy_date;

      $txnid_id = DB::table('premiumusers')->max('txnid');
        if($txnid_id==0){
            $txnid_id=1000;
        }else{
            $txnid_id= $txnid_id+1; 
        }

    $premium->txnid = $txnid_id;
    $premium->status = 2;

    $premium->save();
   }

   public function buyPremiumSuccess(Request $request)
   {

     $user_id=$request->udf1 ;
     $package_amount=$request->amount ;

     $payuMoneyId=$request->payuMoneyId ;
     $mode=$request->mode ;
     $hash=$request->hash ;
     $status=$request->status ;

    $buy_date = date("Y-m-d");
    $order_current_status = 'premium_user';

    $premium = new Premiumuser();
    $premium->user_id = $user_id;
    $premium->package_amount = $package_amount;
    $premium->payu_money_id = $payuMoneyId;
    $premium->buy_date = $buy_date;


    $premium->mode = $mode;
    $premium->payu_money_status = $order_current_status;

    $txnid_id = DB::table('premiumusers')->max('txnid');
        if($txnid_id==0){
            $txnid_id=1000;
        }else{
            $txnid_id= $txnid_id+1; 
        }

    $premium->txnid = $txnid_id;
    $premium->status = 1;

    $premium->save();


    Session::flash('Success','Your have paid successfully for premium user');

    return view('user.projects.buy-premium-success');
   }

   public function buyPremiumFail(Request $request)
   {
     $user_id=$request->udf1 ;
     $package_amount=$request->amount ;

     $payuMoneyId=$request->payuMoneyId ;
     //$mode=$request->mode ;
     $hash=$request->hash ;
     $status=$request->status ;

    $buy_date = date("Y-m-d");
    //$premium_current_status = 'premium_user';

    $premium = new Premiumuser();
    $premium->user_id = $user_id;
    $premium->package_amount = $package_amount;
    $premium->payu_money_id = $payuMoneyId;
    $premium->buy_date = $buy_date;
    //$premium->mode = $mode;
    $premium->payu_money_status = $status;

    $txnid_id = DB::table('premiumusers')->max('txnid');
        if($txnid_id==0){
            $txnid_id=1000;
        }else{
            $txnid_id= $txnid_id+1; 
        }

    $premium->txnid = $txnid_id;
    $premium->status = 0;

    $premium->save();

    Session::flash('error','Your payment for premium user has failed');
      return view('user.projects.buy-premium-fail');
   }

}
