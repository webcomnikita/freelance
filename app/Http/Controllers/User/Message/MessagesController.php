<?php

namespace App\Http\Controllers\User\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Message\Message, App\Models\Message\MessageAttachement, App\Models\Profile\UserProfile, App\Models\Profile\Inquiry, App\Models\Profile\InquiryAttachement, App\Models\Project\Order\Order, App\Models\Project\Order\Ordertransaction, App\Models\User\Premium\Premiumuser;
use App\User;

use App\Http\Requests\MessageAttatchUploadRequest, App\Http\Requests\InquiryFileUploadRequest;

use DB,Helper, Validator, Storage, Auth, Crypt, Redirect,Session;
//use Input;
use Illuminate\Support\Facades\Input;

class MessagesController extends Controller
{
    public function compose(Request $request) {
    	return view('user.messages.compose');
    }

    public function mailApiUsers1(Request $request) {
        $id = $request->id;
        $data=[];
        

        if($request->dump == 'yes') {
             $data['repositories']= Response::json(DB::table('users')->where('status',1)->get(), $status=200, $headers=[], $options=JSON_PRETTY_PRINT);
             return   $data;
        }else{
             $data['repositories']= DB::table('users')->select('id', 'name', 'email')->where('id', '!=', $id)->where('is_email_confirmed',1)->get();
             return   $data;
        }

    }


    public function send(MessageAttatchUploadRequest $request) 
    { 
    	  
        DB::beginTransaction();
        $rcv = $request->input('receiver_id');
        $today1  = date("Y-m-d");
        $arr = [];

        for ($i = 0; $i < count($rcv); $i++) {
            array_push($arr, ['receiver_id' => $rcv[$i],'sender_id' => Auth::user()->id, 'subject' => $request->input('subject'), 'message' => $request->input('message'),'message_date' => $today1]);
        
        }

        $message_ids =[];
        foreach ($arr as $mymesage){

         $message_id= Message::insertGetId($mymesage);
         array_push($message_ids,$message_id);
        }

     try {
         $files = $request->file('files');

           if(!empty($files)){
            //  print(" File Found");
               $arrImageData = [];
               foreach ($files as $photo) { 

                  $filename = strtolower(md5(uniqid().time())).'.'.$photo->getClientOriginalExtension();
                 
                    $destinationPath = config('globals.message_file_store_path');
                    Storage::disk('uploads')->put($destinationPath . $filename ,file_get_contents($photo));

                    array_push($arrImageData,$filename);

                }
                foreach ($message_ids as $my_mesage_id){
                    foreach ($arrImageData as $ImageFileName){
                         $image_data = [];                        
                         $image_data['message_id'] = $my_mesage_id;
                         $image_data['file_path'] = $ImageFileName;
                         MessageAttachement::create($image_data);
                     }
                }


            }
          } catch(ValidationException $e)
          {
              // Back to form
              return Redirect::back();
          }
    
         DB::commit();
          // $msg->save();

        return redirect()->back();

    }

   public function myInbox()
   {
    $user_id = Auth::user()->id;
    $my_inbox = Message::with('sender')
                          ->where('receiver_id', $user_id)->orderByRaw('id DESC')->get();
    $inqry =  Inquiry::with('sender')
                          ->where('sender_user_id', $user_id)->orderByRaw('id DESC')->get();
    $sent =  Message::with('sender')
                          ->where('sender_id', $user_id)->orderByRaw('id DESC')->get();                   

    return view('user.messages.inbox.index',compact('my_inbox','sent','inqry'));
   }

   public function fullInbox($id)
   {
      $msg_id = Message::with('sender')->where('id',$id)->first();
      $m_id = Message::where('id',$id)->first()->id;
      $rcv_id = Message::where('id',$id)->first()->receiver_id;
      $m_attch = MessageAttachement::where('message_id',$m_id)->get();
      $m_attch_count = MessageAttachement::where('message_id',$m_id)->count();
      $rply_count = Message::where('sender_id',$rcv_id)->count('reply_to');

      //$premium_user_id = Auth::user()->id;
      
      
      $premium_user_status = Premiumuser::where('user_id', $rcv_id)->first();
     
     // dd($premium_user_status);die();

      $abc = Premiumuser::where('user_id', $rcv_id)->first();
      if(count($abc)>0)
      {
         $time = strtotime($abc->buy_date);
         $final = date("Y-m-d", strtotime("+1 month", $time));
         //dd($final);die();
      }
      else
      {
         $final = 0;
      }

      return view('user.messages.inbox.single',compact('msg_id','m_attch','m_attch_count','rply_count','premium_user_status','final'));
   }

   public function myInboxReply($id)
   {
      $sndr_id = Message::where('id',$id)->first()->sender_id;
      $msg_id = Message::where('id',$id)->first()->id;
      $rcv_id = Message::where('id',$id)->first()->receiver_id;
      $sub = Message::where('id',$id)->first()->subject;
      $sndr_name = User::where('id',$sndr_id)->first()->name;
      $rply_count = Message::where('sender_id',$rcv_id)->count('reply_to');
      
   
      if ($rply_count<10) {
        return view('user.messages.inbox.reply',compact('sndr_id','rcv_id','sndr_name','sub','msg_id'));
      }
      else{
        Session::flash('error', 'You have buy a premium pack');
        return redirect()->back();
      }
    }

    public function myPremiumInboxReply($id)
   {
      $sndr_id = Message::where('id',$id)->first()->sender_id;
      $msg_id = Message::where('id',$id)->first()->id;
      $rcv_id = Message::where('id',$id)->first()->receiver_id;
      $sub = Message::where('id',$id)->first()->subject;
      $sndr_name = User::where('id',$sndr_id)->first()->name;
      $rply_count = Message::where('sender_id',$rcv_id)->count('reply_to');
   
      return view('user.messages.inbox.reply',compact('sndr_id','rcv_id','sndr_name','sub','msg_id'));
    }

   public function inboxReplySend(MessageAttatchUploadRequest $request)
   {
      
      $msg = new Message();
      $rcv = $request->input('receiver_id');
      $today1  = date("Y-m-d");
      $msg_id = $request->input('reply_to');

      $rply_count = Message::where('sender_id',$rcv)->count('reply_to');
      //dd($rply_count);die();

      //for ($rply_count=0; $rply_count < 3 ; $rply_count++) { 
    
      $msg->receiver_id = Auth::user()->id;
      $msg->sender_id  = $rcv;
      $msg->subject  = $request->input('subject');
      $msg->message  = $request->input('message');
      $msg->message_date  = $today1;
      $msg->reply_to = $msg_id;
      $msg->save();

    //}
  
        $files = $request->file('files');
        $my_mesage_id = $msg->id;
     
           if(!empty($files)){
               $arrImageData = [];
               foreach ($files as $photo) { 

                  $filename = strtolower(md5(uniqid().time())).'.'.$photo->getClientOriginalExtension();
                 
                    $destinationPath = config('globals.message_file_store_path');
                    Storage::disk('uploads')->put($destinationPath . $filename ,file_get_contents($photo));

                    array_push($arrImageData,$filename);

                }
                    foreach ($arrImageData as $ImageFileName){
                         $msg_attch1 = new MessageAttachement();
                         $msg_attch1->message_id= $my_mesage_id;
                         $msg_attch1->file_path= $ImageFileName;
                         $msg_attch1->save();
                     }
             }
        
      
        Session::flash('success', 'You have successfully send your reply');
        return redirect()->route('user.message.my-inbox');

   }

   public function fullInquiry($id)
   {
      $inq_id = Inquiry::with('sender')->where('id',$id)->first();
      $i_id = Inquiry::where('id',$id)->first()->id;
      $i_attch = InquiryAttachement::where('inquiry_id',$i_id)->get();
      $i_attch_count = InquiryAttachement::where('inquiry_id',$i_id)->count();

      return view('user.messages.inquiry.single',compact('inq_id','i_attch','i_attch_count'));
   }

    public function fullSent($id)
   {
     $msg_id = Message::with('sender')->where('id',$id)->first();
      $m_id = Message::where('id',$id)->first()->id;
      $m_attch = MessageAttachement::where('message_id',$m_id)->get();
      $m_attch_count = MessageAttachement::where('message_id',$m_id)->count();

      return view('user.messages.sent.single',compact('msg_id','m_attch','m_attch_count'));
   }

   public function concactMe($slug)
   {

    $sprofiles = UserProfile::where('slug', $slug)->first()->id;              
    return view('user.messages.contact-me',compact('sprofiles'));
   }

   public function inquirySent(InquiryFileUploadRequest $request)
   {

        $inquiry = new Inquiry();

        $pfl = $request->input('profile_id');
        $sndr  = Auth::user()->id;

        $inquiry->profile_id                          = $pfl;
        $inquiry->sender_user_id                      = $sndr;
        $inquiry->message           = $request->input('message');
        $inquiry->save();


        DB::beginTransaction();

        // $inq_id =[];
        // foreach ($inquiry as $mymesage){

        //  $message_id= $inquiry->id;
        //  array_push($inq_id,$message_id);
        // }

     try {
         $files = $request->file('files');
         $my_mesage_id = $inquiry->id;
           if(!empty($files)){
            //  print(" File Found");
               $arrImageData = [];
               foreach ($files as $photo) {                    
              
                $filename       = strtolower(md5(uniqid().time())).'.'.$photo->getClientOriginalExtension();
                 
                    $destinationPath = config('globals.inquiry_file_store_path');
                    Storage::disk('uploads')->put($destinationPath . $filename ,file_get_contents($photo));

                    array_push($arrImageData,$filename);

                }
                // foreach ($inq_id as $my_mesage_id){
                    foreach ($arrImageData as $ImageFileName){
                         $image_data = [];                        
                         $image_data['inquiry_id'] = $my_mesage_id;
                         $image_data['file_path'] = $ImageFileName;
                         InquiryAttachement::create($image_data);
                     }
                // }


            }
          } catch(ValidationException $e)
          {
              // Back to form

              return Redirect::back();
          }
    
         DB::commit();

         $reg_id = Auth::user()->gcm;

         $title = 'Inquiry notification';
         $body = 'Inquiry body';

         $inquiryMsg = Helper::sendGcmNotify($reg_id,$title,$body);

        return redirect()->back();

     
   }

   public function orderNow($slug)
   {

    $a = UserProfile::where('slug',$slug)->first();

    $priceVal = 1;
    $MERCHANT_KEY = 'eFPOJqhW';
    $SALT = 'Q8kM5RTT5L';
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

    $email = 'abc@test.test' ;
    $name = 'nikita' ;
    $mobile_no = '8767654343';
    $location = 'guwahati';
    $zip_code = '789876';
    $udf1=$a->user_id ;  // Seller Id
    $udf2=$a->id ;  //  serller profile Id
    $udf3=$a->package_amount;

    $hash         = '';
    $productinfo  = 'Ad Fees';

    $hash_string = $MERCHANT_KEY."|".$txnid."|".$priceVal."|".$productinfo."|".$name."|".$email."|".$udf1."|".$udf2."|".$udf3."||||||||".$SALT;

   // hashSequence = key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||salt.
    $hash = strtolower(hash('sha512', $hash_string));

    return view('user.profiles.order-now', compact('MERCHANT_KEY','hash','txnid','hash_string','priceVal','name','email','mobile_no','location','zip_code','productinfo','a','udf1','udf2','udf3'));
   }

   public function orderNowInit(Request $request)
   {

      $seller_id = $request->input('seller_id');
      $user_profile_id = $request->input('user_profile_id');
      $project_cost = $request->input('project_cost');
      $buyer_id = Auth::user()->id;
      $order_date = date("Y-m-d");

      $ordert = new Ordertransaction();
      $ordert->buyer_id = $buyer_id;
      $ordert->seller_id = $seller_id;
      $ordert->user_profile_id = $user_profile_id;
      $ordert->project_cost = $project_cost;
      $ordert->order_date = $order_date;

      $txnid_id = DB::table('ordertransactions')->max('txnid');
        if($txnid_id==0){
            $txnid_id=1000;
        }else{
            $txnid_id= $txnid_id+1; 
        }

    $ordert->txnid = $txnid_id;
    $ordert->status = 2;

    $ordert->save();

      
   }


   // public function TestRedirect( Request $request)
   // {
   //     return Redirect::away('https://sandboxsecure.payu.in/_payment')->withInputs(Input::all());
   // }

   public function orderNowSuccess(Request $request)
   {
     $seller_id=$request->udf1 ;
     $user_profile_id=$request->udf2 ;
     $project_cost=$request->udf3 ;

     $payuMoneyId=$request->payuMoneyId ;
     $mode=$request->mode ;
     $hash=$request->hash ;
     $status=$request->status ;

    $buyer_id = Auth::user()->id;
    $order_date = date("Y-m-d");
    $order_current_status = 'order_successful';

    $ordert = new Ordertransaction();
    $ordert->buyer_id = $buyer_id;
    $ordert->seller_id = $seller_id;
    $ordert->user_profile_id = $user_profile_id;
    $ordert->project_cost = $project_cost;
    $ordert->order_date = $order_date;

    $ordert->payu_money_id = $payuMoneyId;
    $ordert->mode = $mode;
    $ordert->payu_money_status = $order_current_status;

    $txnid_id = DB::table('ordertransactions')->max('txnid');
        if($txnid_id==0){
            $txnid_id=1000;
        }else{
            $txnid_id= $txnid_id+1; 
        }

    $ordert->txnid = $txnid_id;
    $ordert->status = 1;

    $ordert->save();

    $order = new Order();
    $order->order_transaction_id = $ordert->id;
    $order->buyer_id = $ordert->buyer_id;
    $order->seller_id = $ordert->seller_id;
    $order->user_profile_id = $ordert->user_profile_id;
    $order->project_cost = $ordert->project_cost;
    $order->order_date = $ordert->order_date;
    $order->save();

    Session::flash('Success','Your payment transaction has successfull');


    return view('user.profiles.order-now-success');
   }



   public function orderNowFail(Request $request)
   {

    $seller_id=$request->udf1 ;
    $user_profile_id=$request->udf2 ;
    $project_cost=$request->udf3 ;


    $buyer_id = Auth::user()->id;

    $order_date = date("Y-m-d");

     $payuMoneyId=$request->payuMoneyId ;
     $order_current_status=$request->status ;

    $ordert = new Ordertransaction();
    $ordert->buyer_id = $buyer_id;
    $ordert->seller_id = $seller_id;
    $ordert->user_profile_id = $user_profile_id;
    $ordert->project_cost = $project_cost;
    $ordert->order_date = $order_date;
    $ordert->payu_money_id = $payuMoneyId;
    $ordert->payu_money_status = $order_current_status;

    $txnid_id = DB::table('ordertransactions')->max('txnid');
        if($txnid_id==0){
            $txnid_id=1000;
        }else{
            $txnid_id= $txnid_id+1; 
        }

    $ordert->txnid = $txnid_id;
    $ordert->status = 0;

    $ordert->save();

    $order = new Order();
    $order->order_transaction_id = $ordert->id;
    $order->buyer_id = $ordert->buyer_id;
    $order->seller_id = $ordert->seller_id;
    $order->user_profile_id = $ordert->user_profile_id;
    $order->project_cost = $ordert->project_cost;
    $order->order_date = $ordert->order_date;
    

    $order->save();

    Session::flash('Failed','Your payment transaction has failed');

    return view('user.profiles.order-now-fail');
   }

}
