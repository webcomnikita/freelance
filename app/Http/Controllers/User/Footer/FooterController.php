<?php

namespace App\Http\Controllers\User\Footer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function aboutCompany()
    {
        return view('footer-link.about-company');
    }

    public function business()
    {
        return view('footer-link.business');
    }

    public function pmpSupport()
    {
        return view('footer-link.pmp-support');
    }

    public function createSeller()
    {
        return view('footer-link.seller-profile');
    }

    public function postProject()
    {
        return view('footer-link.post-project');
    }

    public function postingTerm()
    {
        return view('footer-link.posting-terms');
    }

    public function policy()
    {
        return view('footer-link.policy');
    }

    public function term()
    {
        return view('footer-link.terms');
    }

    public function fees()
    {
        return view('footer-link.fees');
    }


}
