<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Profile\UserProfile;
use App\User;
use Session;

class SellerUserDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seller = UserProfile::with('user')->with('category')->orderByRaw('id DESC')->get();
        // $seller = UserProfile::all();
        return view('admin.master.seller_user_detail.index',compact('seller'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = $request->input('user_id');
        $profile_id = $request->input('profile_id');

        $count =  UserProfile::where('user_id',$user_id)->where('status',1)->count();

        $status_id = UserProfile::where('user_id',$user_id)->first()->id;

        if($count<5) {

           
            $status = UserProfile::where('user_id',$user_id)->where('id',$profile_id)->first();
            $status->status = 1;
            $status->save();

            Session::flash('success', 'You have successfully approved user seller account');

            return redirect()->back();
           
        }
        else{
            Session::flash('error', 'You can approve only 5 seller accounts');

            return redirect()->back();
        }
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $seller = UserProfile::with('user')->with('category')->where('slug',$slug)->first();
        return view('admin.master.seller_user_detail.single',compact('seller'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function undoStore(Request $request)
    {
        $user_id = $request->input('user_id');

        $status = UserProfile::find($user_id);
        $status->status = 0;
        $status->save();

        Session::flash('success', 'You have successfully change seller from approved user to not approved user');

        return redirect()->back();


    }
}
