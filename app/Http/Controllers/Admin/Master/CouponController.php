<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Coupon\Coupon;
use Session;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::all();
        return view('admin.master.coupon.index',compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coupon = new Coupon();

        $coupon->coupon_name  = $request->input('coupon_name');
        $coupon->coupon_name_slug = str_slug($request->input('coupon_name'), '-');
        $coupon->coupon_start_date  = $request->input('coupon_start_date');
        $coupon->coupon_end_date  = $request->input('coupon_end_date');
        $coupon->coupon_discount_amount  = $request->input('coupon_discount_amount');
        $coupon->save();

        Session::flash('success','You have successfully added coupon details');
        return redirect()->route('admin.coupon.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::find($id);
        return view('admin.master.coupon.edit',compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $coupon = Coupon::find($id);
        $coupon->coupon_name  = $request->input('coupon_name');
        $coupon->coupon_name_slug = str_slug($request->input('coupon_name'), '-');
        $coupon->coupon_start_date  = $request->input('coupon_start_date');
        $coupon->coupon_end_date  = $request->input('coupon_end_date');
        $coupon->coupon_discount_amount  = $request->input('coupon_discount_amount');
        $coupon->save();

        Session::flash('success','You have successfully updated coupon details');
        return redirect()->route('admin.coupon.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function disable($id)
    {
        $coupon = Coupon::find($id);
        $coupon->coupon_disable = 1;
        $coupon->save();
        
        Session::flash('success','You have successfully disabled coupon details');
        return redirect()->route('admin.coupon.index');
    }
}
