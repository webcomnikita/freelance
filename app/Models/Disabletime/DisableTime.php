<?php

namespace App\Models\Disabletime;

use Illuminate\Database\Eloquent\Model;

class DisableTime extends Model
{
    protected $fillable = array('time_in_hr');
	protected $table    = 'disable_times';
    protected $guarded  = ['_token'];

    public static $rules = [
        'time_in_hr'   			=>  'required',
    ];
}
