<?php

namespace App\Models\Coupon;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = array('coupon_name','coupon_name_slug', 'coupon_start_date','coupon_end_date', 'coupon_discount_amount' );
	protected $table    = 'coupons';
    protected $guarded  = ['_token'];

    public static $rules = [
        'coupon_name'   			=>  'required|unique:coupons|max:255',
    	'coupon_start_date' 		=>  'required|date_format:Y-m-d H:i:s',
    	'coupon_end_date' 			=>  'required|date_format:Y-m-d H:i:s',
    	'coupon_discount_amount' 	=>  'required',
    ];
}
