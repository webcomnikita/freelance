<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class UserProfilePortfolio extends Model
{
    protected $fillable = array('user_profile_id', 'title', 'slug', 'description','image_path','web_link','status');
	protected $table    = 'user_profile_portfolios';
    protected $guarded  = ['_token'];

    // public static $rules = [
    //     'user_profile_id'   	=>  'required|exists:user_profiles,id',
    // 	'title' 				=>  'required', 
    // 	'description'  			=>  'required',
    // 	'image_path' 			=>  'required',
    // 	'web_link',				=>	'required',
    // ];
}
