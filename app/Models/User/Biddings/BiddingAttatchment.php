<?php

namespace App\Models\User\Biddings;

use Illuminate\Database\Eloquent\Model;

class BiddingAttatchment extends Model
{
    protected $fillable = array('bidding_id', 'file_path' );
  protected $table    = 'bidding_attatchments';
  protected $guarded  = ['_token'];

  public static $rules = [
    'bidding_id'   	=>  'required|exists:biddings,id',
    'file_path' 	 =>  'required',
  ];

  public function bidding()
  {
      return $this->belongsTo('App\Models\User\Biddings\Bidding', 'bidding_id');
  }
}
