<?php

namespace App\Models\User\Biddings;

use Illuminate\Database\Eloquent\Model;

class Bidding extends Model
{
    protected $fillable = array('sender_id', 'receiver_id','subject','message', 'message_date','bidding_amount' );
    protected $table    = 'biddings';
    protected $guarded  = ['_token'];

    public static $rules = [
        'sender_id'     =>  'required|exists:users,id',
        'receiver_id'   =>  'required|exists:users,id',
        'message'       =>  'required|min:4',
        'message_date'  =>  'required|date_format:Y-m-d H:i:s'
    ];
}
