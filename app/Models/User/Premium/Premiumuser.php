<?php

namespace App\Models\User\Premium;

use Illuminate\Database\Eloquent\Model;

class Premiumuser extends Model
{
   protected $fillable = [
        'user_id', 'package_amount','buy_date','premium_end_date','txnid','status',
    ];

    public static $rules = [
        'user_id'   		=> 'required',
        'amount'  		=> 'required',
        'buy_date'  	=> 'required',
        'txnid'  	=> 'required',
        'status' 	 	=> 'required|date_format:Y-m-d',

    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
