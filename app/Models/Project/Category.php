<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = array('name', 'slug', 'icon_path', 'status');
	   protected $table    = 'categories';
     protected $guarded  = ['_token'];

    public static $rules = [
    	'name' 	=> 'required|max:255|unique:categories',
    	'slug' 		=> 'required|max:255|unique:categories',
		'icon_path' 	=> 'image|mimes:jpeg,bmp,png|max:2000',
		'status'			=> 'required',
    ];

    public function project()
    {
        return $this->hasMany('App\Models\Project\Project');
    }
}
