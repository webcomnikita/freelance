<?php

namespace App\Models\Project\Order;

use Illuminate\Database\Eloquent\Model;

class Ordertransaction extends Model
{
    protected $fillable = [
        'buyer_id', 'seller_id','user_profile_id','project_cost','order_date',
    ];

    public static $rules = [
        'buyer_id'   		=> 'required',
        'seller_id'  		=> 'required',
        'user_profile_id'  	=> 'required',
        'project_cost'  	=> 'required',
        'order_date' 	 	=> 'required|date_format:Y-m-d',

    ];
}
