<footer class="main-footer">
   <div class="footer-content">
      <div class="container">
         <div class="row">
            <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-6 ">
               <div class="footer-col">
                  <h4 class="footer-title">About us</h4>
                  <ul class="list-unstyled footer-nav">
                     <li><a target="_blank" href="{{ route('user.footer-about-company') }}">About Company</a></li>
                     <li><a target="_blank" href="{{ route('user.footer-for-business') }}">For Business</a></li>
                     <li><a target="_blank" href="{{ route('user.footer-pmp-support') }}">Pmp Support</a></li>
                  </ul>
               </div>
            </div>
            <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-6 ">
               <div class="footer-col">
                  <h4 class="footer-title">Help & Contact</h4>
                  <ul class="list-unstyled footer-nav">
                     <li><a target="_blank" href="{{ route('user.footer-seller-profile') }}">
                        How To Create Seller Profile
                        </a>
                     </li>
                     <li><a target="_blank" href="{{ route('user.footer-post-a-project') }}">
                        How To Post A Project</a>
                     </li>
                     <li><a target="_blank" href="{{ route('user.footer-posting-terms') }}">
                        Posting Terms
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-6 ">
               <div class="footer-col">
                  <h4 class="footer-title">Our Terms</h4>
                  <ul class="list-unstyled footer-nav">
                      <li><a target="_blank" href="{{ route('user.footer-terms') }}">
                        Terms Of Use
                        </a>
                     </li>
                     <li><a target="_blank" href="{{ route('user.footer-policy') }}"> Privacy Policy
                        </a>
                     </li>
                     <li><a target="_blank" href="{{ route('user.footer-fees-and-charges') }}"> Fees And Charges
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-6 ">
               <div class="footer-col">
                  <h4 class="footer-title">Account</h4>
                  <ul class="list-unstyled footer-nav">
                     @if (Auth::check())
                     <li><a target="_blank" href="{{ route('dashboard') }}">Dashboard
                        </a>
                     </li>
                     <li><a target="_blank" href="{{ route('browse-jobs') }}"> Browse job
                        </a>
                     </li>
                     <li><a target="_blank" href="{{ route('user.my-profile') }}"> Profile
                        </a>
                     </li>
                     @else
                     <li><a target="_blank" href="{{ route('user.login') }}">Login
                        </a>
                     </li>
                     <li><a target="_blank" href="{{ route('user.register') }}">Register
                        </a>
                     </li>
                     <li><a target="_blank" href="{{ route('projects.create') }}">Post a job
                        </a>
                     </li>
                     @endif
                     
                     
                  </ul>
               </div>
            </div>
            <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12">
               <div class="footer-col row">
                  <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                     <div class="mobile-app-content">
                        <h4 class="footer-title">Mobile Apps</h4>
                        <div class="row ">
                           <div class="col-xs-12 col-sm-6 ">
                              <a class="app-icon" target="_blank" href="https://itunes.apple.com/">
                              <span class="hide-visually">iOS app</span>
                              <img src="{!!asset ('public-assets/images/site/app_store_badge.svg') !!}" alt="Available on the App Store">
                              </a>
                           </div>
                           <div class="col-xs-12 col-sm-6 ">
                              <a class="app-icon" target="_blank" href="https://play.google.com/store/">
                              <span class="hide-visually">Android App</span>
                              <img src="{!!asset ('public-assets/images/site/google-play-badge.svg') !!}" alt="Available on the App Store">
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                     <div class="hero-subscribe">
                        <h4 class="footer-title no-margin">Follow us on</h4>
                        <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
                           <li><a class="icon-color fb" title="Facebook" data-placement="top" data-toggle="tooltip" href="#"><i class="fa fa-facebook"></i> </a></li>
                           <li><a class="icon-color tw" title="Twitter" data-placement="top" data-toggle="tooltip" href="#"><i class="fa fa-twitter"></i> </a></li>
                           <li><a class="icon-color gp" title="Google+" data-placement="top" data-toggle="tooltip" href="#"><i class="fa fa-google-plus"></i> </a></li>
                           <li><a class="icon-color lin" title="Linkedin" data-placement="top" data-toggle="tooltip" href="#"><i class="fa fa-linkedin"></i> </a></li>
                           <li><a class="icon-color pin" title="Linkedin" data-placement="top" data-toggle="tooltip" href="#"><i class="fa fa-pinterest-p"></i> </a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div style="clear: both"></div>
            <div class="col-lg-12">
               <div class=" text-center paymanet-method-logo">
                  <img src="{!!asset ('public-assets/images/site/payment/master_card.png') !!}" alt="img">
                  <img alt="img" src="{!!asset ('public-assets/images/site/payment/visa_card.png') !!}">
                  <img alt="img" src="{!!asset ('public-assets/images/site/payment/paypal.png') !!}">
                  <img alt="img" src="{!!asset ('public-assets/images/site/payment/american_express_card.png') !!}"> 
                  <img alt="img" src="{!!asset ('public-assets/images/site/payment/discover_network_card.png') !!}">
                  <img alt="img" src="{!!asset ('public-assets/images/site/payment/google_wallet.png') !!}">
               </div>
               <div class="copy-info text-center">
                  &copy; 2017 Pivk My Project. All Rights Reserved.
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>