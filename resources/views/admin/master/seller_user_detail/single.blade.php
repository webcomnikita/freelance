    @extends('admin.layout.main')
    @section('page-header')  {{ ucwords($seller->profile_title) }} Profile Detail @stop

    @section('page-css')
        <style>
            .img-bordered {
                height: 60px;
                width: 60px;
                border: .5px solid #222;
            }
        </style>
    @stop

    @section('main-content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ ucwords($seller->profile_title) }} Profile Detail 
                    </h2>
                </div>

                <div class="body">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#home_animation_2" data-toggle="tab">User detail</a></li>
                                <li role="presentation"><a href="#profile_animation_2" data-toggle="tab">Seller profile detail</a></li>
                               {{--  <li role="presentation"><a href="#messages_animation_2" data-toggle="tab">MESSAGES</a></li>
                                <li role="presentation"><a href="#settings_animation_2" data-toggle="tab">SETTINGS</a></li> --}}
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane animated fadeInRight active" id="home_animation_2">
                                    <b>User Name</b>
                                    <p>
                                       {{ $seller->user->name }}
                                    </p>

                                    <b>User Email</b>
                                    <p>
                                       {{ $seller->user->email }}
                                    </p>

                                    <b>User Member Since</b>
                                    <p>
                                       {{ date('d-M-Y', strtotime($seller->user->member_since)) }}
                                    </p>

                                    <b>User Profile Image</b>
                                    <p>
                                       @if($seller->profile_image)
                                       <img class="img-bordered" src="{{ url('uploads/profiles/images/'.$seller->profile_image) }}" alt="">
                                       @else
                                       No profile photo has uploaded
                                       @endif
                                    </p>

                                    <b>User Mobile Number</b>
                                    <p>
                                       @if($seller->user->mobile_number)
                                       {{ $seller->user->mobile_number }}
                                       @else
                                       ---
                                       @endif
                                    </p>

                                    <b>Is User A Premium Member</b>
                                    <p>
                                        @if($seller->user->premium_member == 1)
                                        <span class="label label-success">Premium user</span>
                                        @else
                                        <span class="label label-danger">Not a premium user</span>
                                        @endif
                                    </p>

                                </div>
                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">
                                    <b>Profile Name</b>
                                    <p>
                                       {{ ucwords($seller->profile_title) }} 
                                    </p>

                                    <b>Profile Description</b>
                                    <p>
                                       {!! $seller->profile_description !!} 
                                    </p>

                                    <b>Profile Category</b>
                                    <p>
                                       {{ $seller->category->name }} 
                                    </p>

                                    <b>Profile Package Amount</b>
                                    <p>
                                        <i class="fa fa-rupee"> {{ $seller->package_amount }}</i> 
                                    </p>

                                    <b>Profile Package Description</b>
                                    <p>
                                        {!! $seller->package_description !!} 
                                    </p>

                                    <b>Profile Status</b>
                                    <p>
                                        @if($seller->status == 1)
                                        <span class="label label-success">Active profile</span>
                                        @else
                                        <span class="label label-danger">Yet not activate this profile</span>
                                        @endif
                                    </p>

                                    <hr>
                                    

                                    @if($seller->status == 0)
                                        <form action="{{ url('admin/seller-user-details/save') }}" method="post" class="apprv">
                                        {{ csrf_field() }}
                                            <input type="hidden" name="user_id" value="{{ $seller->user_id }}">
                                            <input type="hidden" name="profile_id" value="{{ $seller->id }}">
                                            <button type="hidden" class="btn btn-warning"><i class="fa fa-toggle-on"></i>&nbsp; Activete this user</button>
                                        </form>
                                        @else
                                        <form action="{{ url('admin/seller-user-details/undoSave') }}" method="post" class="napprv">
                                        {{ csrf_field() }}
                                            <input type="hidden" name="user_id" value="{{ $seller->user_id }}">
                                            <input type="hidden" name="profile_id" value="{{ $seller->id }}">
                                            <button type="hidden" class="btn btn-primary"><i class="fa fa-toggle-on"></i>&nbsp; Dctivete this user</button>
                                        </form>
                                    @endif
                                </div>
                               {{--  <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                    <b>Message Content</b>
                                    <p>
                                        Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                                        Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent
                                        aliquid pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere
                                        gubergren sadipscing mel.
                                    </p>
                                </div>
                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_animation_2">
                                    <b>Settings Content</b>
                                    <p>
                                        Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                                        Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent
                                        aliquid pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere
                                        gubergren sadipscing mel.
                                    </p>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div> 

            </div>  
        </div>
    </div>

    @endsection

    @section('page-js')

    @stop


