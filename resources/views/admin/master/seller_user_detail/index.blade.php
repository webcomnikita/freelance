@extends('admin.layout.main')
@section('page-header') All Seller User Profile Detail @stop

@section('page-css')
<style>
    .img-bordered {
        height: 50px;
        width: 50px;
        border: .5px solid #222;
        border-radius: 2px;
    }
</style>
@stop

@section('main-content')

<!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               All seller user profile detail
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                @if(count($seller))
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Seller User name</th>
                                            <th>Seller Profile Name</th>
                                            <th>Seller User email</th>
                                            <th>Seller User profile photo</th>
                                            <th>Seller Member since</th>
                                            <th>Mobile no</th>
                                            <th>Status</th>
                                            <th>Acions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Seller User name</th>
                                            <th>Seller Profile Name</th>
                                            <th>Seller User email</th>
                                            <th>Seller User profile photo</th>
                                            <th>Seller Member since</th>
                                            <th>Mobile no</th>
                                            <th>Status</th>
                                            <th>Acions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php $i=1; ?>
                                       @foreach($seller as $user) 
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ ucwords($user->user->name)  }}</td>
                                            <td>{{ ucwords($user->profile_title ) }}</td>
                                            <td>{{ $user->user->email }}</td>
                                            <td>
                                                <img class="img-bordered" src="{{ url('uploads/profiles/images/'.$user->profile_image) }}" alt="">
                                            </td>
                                            <td>{{ date('d-M-Y', strtotime($user->user->member_since)) }}</td>
                                            <td>{{ $user->user->mobile_number }}</td>
                                             <td>
                                                @if($user->status == 0)
                                                <span class="label label-danger">Not approved</span>
                                                @else
                                                <span class="label label-success">Approved</span>
                                                @endif
                                            </td>
                                            <!-- <td>
                                                @if($user->status == 0)
                                                <form action="{{ url('admin/seller-user-details/save') }}" method="post" class="apprv">
                                                {{ csrf_field() }}
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                    <button type="hidden" class="btn btn-warning"><i class="fa fa-toggle-on"></i></button>
                                                </form>
                                                @else
                                                <form action="{{ url('admin/seller-user-details/undoSave') }}" method="post" class="napprv">
                                                {{ csrf_field() }}
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                    <button type="hidden" class="btn btn-primary"><i class="fa fa-toggle-on"></i></button>
                                                </form>
                                                @endif
                                            </td> -->

                                            <td><a href="{{ url('admin/seller-user-details/profile-detail', $user->slug ) }}" class="btn btn-warning"><i class="fa fa-info"></i></a></td>
                                        </tr>
                                       <?php $i++ ?>
                                       @endforeach
                                    </tbody>
                                </table>
                                 @else
                                    <div class="alert alert-danger">
                                      <strong>Oops !</strong> No Seller User Detail Found.
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
@endsection

@section('page-js')
<script>
    $(".apprv").on("submit", function(){
        return confirm("Want to approve seller account ?");
    });
    $(".napprv").on("submit", function(){
        return confirm("Want to undo approve to not approved seller account ?");
    });
</script>
@stop


