@extends('admin.layout.main')

@section('main-content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    All disable times added
                    <small>View/Edit disable time</small>
                </h2>
                
            </div>
            <div class="body table-responsive">
                @if(count($d_time))
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>TIME (in hour)</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($d_time as $k => $v)
                        <tr>
                            <th scope="row">{{ $k+1 }}</th>
                            <td>{{ $v->time_in_hr }}</td>
                            <td>
                                <a href="{{ route('admin.disable_time.edit',$v->id) }}" class="btn btn-sm btn-warning waves-effect"><i class="fa fa-pencil-square-o" aria-hidden="true" ></i> Edit</a>
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
                @else
                    <div class="alert alert-danger">
                      <strong>Oops !</strong> No disable times Found.
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@stop

@section('page-header') All disable times @stop
