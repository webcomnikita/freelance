@extends('admin.layout.main')

@section('main-content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Disable time
                    <small>Edit Disable time</small>
                </h2>
                
            </div>
            <div class="body">
                
                <div class="row clearfix">
                    <div class="col-md-12">
                        
                        <form action="{{ route('admin.disable_time.update',$time->id) }}" method="post">
                            {{ csrf_field() }}
                            {!! method_field('PATCH') !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <div class="form-line">
                                            <input type="text" name="time_in_hr" class="form-control required" id="time_in_hr" autocomplete="off" placeholder="Enter coupon name" value="{{ old('time_in_hr',$time->time_in_hr) }}">
                                         </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <button class="btn btn-primary waves-effect btn-block" type="submit" style="margin-top: 1em;">Update</button>
                                    </div>
                                </div>

                            </div>

                            

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-header') Edit Disable time @stop
