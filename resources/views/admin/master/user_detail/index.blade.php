@extends('admin.layout.main')
@section('page-header') All User Detail @stop

@section('page-css')

@stop

@section('main-content')

<!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               All user detail
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                @if(count($user_detail))
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>USER NAME</th>
                                            <th>USER EMAIL</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>USER NAME</th>
                                            <th>USER EMAIL</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php $i=1; ?>
                                       @foreach($user_detail as $user) 
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ ucwords($user->name)  }}</td>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                       <?php $i++ ?>
                                       @endforeach
                                    </tbody>
                                </table>
                                 @else
                                    <div class="alert alert-danger">
                                      <strong>Oops !</strong> No User Detail Found.
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
@endsection

@section('page-js')

@stop


