@extends('admin.layout.main')

@section('main-content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    All Coupons Added
                    <small>View/Edit Coupon</small>
                </h2>
                
            </div>
            <div class="body table-responsive">
                @if(count($coupons))
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>NAME</th>
                            <th>START DATE</th>
                            <th>END DATE</th>
                            <th>DISCOUNT AMOUNT</th>
                            <th>DISABLED</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($coupons as $k => $v)
                        <tr>
                            <th scope="row">{{ $k+1 }}</th>
                            <td>{{ $v->coupon_name }}</td>
                            <td>{{ date('d-M-Y', strtotime($v->coupon_start_date)) }}</td>
                            <td>{{ date('d-M-Y', strtotime($v->coupon_end_date)) }}</td>
                            <td><i class="fa fa-rupee"></i>&nbsp;{{ $v->coupon_discount_amount }}</td>
                            <td>
                                @if($v->coupon_disable == 1)
                                    <span class="label label-success">Visible</span>
                                @else
                                    <span class="label label-danger">Disabled</span>
                                @endif

                            </td>
                            <td>
                                
                                    <a href="{{ route('admin.coupon.edit',$v->id) }}" class="btn btn-sm btn-warning waves-effect"><i class="fa fa-pencil-square-o" aria-hidden="true" ></i> Edit</a>

                                    <form action="{{ route('admin.coupon.disable',$v->id) }}" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-danger waves-effect" onclick="return confirm('Want to change disable status ?')"> <i class="fa fa-ban" aria-hidden="true"></i> Disable</button>
                                    </form>
                                
                             </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
                @else
                    <div class="alert alert-danger">
                      <strong>Oops !</strong> No Categories Found.
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@stop

@section('page-header') All Coupons @stop
