@extends('admin.layout.main')

@section('main-content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Coupon
                    <small>Edit Coupon</small>
                </h2>
                
            </div>
            <div class="body">
                
                <div class="row clearfix">
                    <div class="col-md-12">
                        
                        <form action="{{ route('admin.coupon.update',$coupon->id) }}" method="post">
                            {{ csrf_field() }}
                            {!! method_field('PATCH') !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <div class="form-line">
                                            <input type="text" name="coupon_name" class="form-control required" id="coupon_name" autocomplete="off" placeholder="Enter coupon name" value="{{ old('coupon_name',$coupon->coupon_name) }}">
                                         </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="form-line">
                                            <input type="date" name="coupon_start_date" class="form-control required" id="coupon_start_date" autocomplete="off" placeholder="Enter coupon start date" value="{{ old('coupon_start_date',$coupon->coupon_start_date) }}">
                                         </div>
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                     <div class="form-group ">
                                        <div class="form-line">
                                            <input type="tel" name="coupon_discount_amount" class="form-control required" id="coupon_discount_amount" autocomplete="off" placeholder="Enter coupon discount amount" value="{{ old('coupon_discount_amount',$coupon->coupon_discount_amount) }}">
                                         </div>
                                    </div>

                                     <div class="form-group ">
                                        <div class="form-line">
                                            <input type="date" name="coupon_end_date" class="form-control required" id="coupon_end_date" autocomplete="off" placeholder="Enter coupon end date" value="{{ old('coupon_end_date',$coupon->coupon_end_date) }}">
                                         </div>
                                    </div>        
                                </div>

                            </div>

                            <div class="form-group form-float">
                                <button class="btn btn-primary waves-effect btn-block" type="submit" style="margin-top: 1em;">Update</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-header') Edit coupon @stop
