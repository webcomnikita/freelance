<!-- User Info -->
<div class="user-info">
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hi, {{ Auth::guard('admin')->user()->name }}</div>
        <div class="email">{{ Auth::guard('admin')->user()->username }}</div>
        <div class="btn-group user-helper-dropdown">
            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
            <ul class="dropdown-menu pull-right">
               
                {{-- <li role="seperator" class="divider"></li> --}}
                <li><a href="{{ route('admin.logout') }}"><i class="material-icons">input</i>Sign Out</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- #User Info -->
<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
            <a href="{{ url('admin/home') }}">
                <i class="material-icons">home</i>
                <span>Dashboard</span>
            </a>
        </li>
        <li>
            <a href="{{ url('admin/user-details') }}">
                <i class="material-icons">layers</i>
                <span>User emails</span>
            </a>
        </li>

        <li>
            <a href="{{ route('admin.disable_time.index') }}">
                <i class="material-icons">layers</i>
                <span>Disable times</span>
            </a>
        </li>

        <li>
            <a href="{{ url('admin/seller-user-details') }}">
                <i class="material-icons">layers</i>
                <span>User Seller profiles</span>
            </a>
        </li>
        
        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">layers</i>
                <span>Project category</span>
            </a>
            <ul class="ml-menu">
                <li>
                    <a href="{{ url('admin/category/create') }}">Add category</a>
                </li>
                <li>
                    <a href="{{ url('admin/category') }}">View all category</a>
                </li>
             </ul>
        </li>

        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">layers</i>
                <span>Project sub category</span>
            </a>
            <ul class="ml-menu">
                <li>
                    <a href="{{ url('admin/sub-category/create') }}">Add sub-category</a>
                </li>
                <li>
                    <a href="{{ url('admin/sub-category') }}">View all sub category</a>
                </li>
             </ul>
        </li>

        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">layers</i>
                <span>Coupon</span>
            </a>
            <ul class="ml-menu">
                <li>
                    <a href="{{ route('admin.coupon.create') }}">Add coupon</a>
                </li>
                <li>
                    <a href="{{ route('admin.coupon.index') }}">View all coupons</a>
                </li>
             </ul>
        </li>
     
    </ul>
</div>
<!-- #Menu -->
<!-- Footer -->
<div class="legal">
    <div class="copyright">
        &copy; 2017-2018 <a href="javascript:void(0);">MAIONG</a>.
    </div>
    <div class="version">
        <b>Version: </b> 1.0
    </div>
</div>