@extends('ui.maiong_ui.main')

@section('pageCss')


<style>
  
   .profile-box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
   }
   .mt1 {
      margin-top: 1em;
   }
   /* Tabs panel */
.tabbable-panel {
  border:1px solid #eee;
  padding: 10px;
}

/* Default mode */
.tabbable-line > .nav-tabs {
  border: none;
  margin: 0px;
}
.tabbable-line > .nav-tabs > li {
  margin-right: 2px;
}
.tabbable-line > .nav-tabs > li > a {
  border: 0;
  margin-right: 0;
  color: #737373;
}
.tabbable-line > .nav-tabs > li > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
  border-bottom: 4px solid #fbcdcf;
}
.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
  border: 0;
  background: none !important;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
  margin-top: 0px;
}
.tabbable-line > .nav-tabs > li.active {
  border-bottom: 4px solid #16a085;
  position: relative;
}
.tabbable-line > .nav-tabs > li.active > a {
  border: 0;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.active > a > i {
  color: #404040;
}
.tabbable-line > .tab-content {
  margin-top: -3px;
  background-color: #fff;
  border: 0;
  border-top: 1px solid #eee;
  padding: 15px 0;
}
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}


.tgreen {
   color: #16a085;
   font-weight: bold;
}

</style>
@stop

@section('main_content')

<div class="row">
   <div class="col-md-12 mt1">
      <div class="profile-box">
    <h2 class="title-2"><i class="icon-user-add"></i> Order summery </h2>

         <div class="row">
            <div class="col-md-12">

                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th style="width: 25%;">Status</th>
                            <td>
                              @if($seller->order_current_status == 'order_placed')
                              <label class="label label-warning">Pending</label>
                              <label class="label label-success" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">Accept order</label>
                            </td>
                              @else
                              <label class="label label-success">Processing</label></td>
                              @endif
                          </tr>
                          <tr>
                            <th style="width: 25%;">Buyer</th>
                            <td>{{ ucwords($seller->buyer->name) }}</td>
                          </tr>
                          <tr>
                            <th style="width: 25%;">Order time</th>
                            <td>{{ date('d-M-Y', strtotime($seller->order_date)) }}</td>
                          </tr>
                          <tr>
                            <th style="width: 25%;">Delivery</th>
                            <td>
                              @if($seller->expected_delivery_date)
                              <span>{{ date('d-M-Y', strtotime($seller->expected_delivery_date)) }} (Expected)</span>
                              @else
                              <span>---</span>
                              @endif
                          </td>
                          </tr>
                          <tr>
                            <th style="width: 25%;">Bill amount</th>
                            <td><i class="fa fa-rupee"></i>&nbsp;{{ $seller->project_cost }}</td>
                          </tr>
                        </tbody>
                      </table>

                    <hr>

                      

            </div>
         </div>

      </div>
   </div> 
</div> 

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <form action="{{ route('user.my-activity-seller.post') }}" method="post" >
          {{ csrf_field() }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Project delivery date (expected)</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              
              <input type="hidden" name="order_id" value="{{ $seller->id }}">
              
              <label>Project expected date</label>
              <input type="date" name="expected_delivery_date" class="form-control" required="required">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button class="btn btn-success pull-right" id="accpt" type="submit">Accept order</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>

@endsection

@section('pageJs')

<script>
 $(document).ready(function() {
   
  });

 $("#accpt").on("click", function(){
        return confirm("Want to accept project order ?");
  });
</script>
@stop

