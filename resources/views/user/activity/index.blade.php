@extends('ui.maiong_ui.main')

@section('pageCss')


<style>
  
   .profile-box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
   }
   .mt1 {
      margin-top: 1em;
   }
   /* Tabs panel */
.tabbable-panel {
  border:1px solid #eee;
  padding: 10px;
}

/* Default mode */
.tabbable-line > .nav-tabs {
  border: none;
  margin: 0px;
}
.tabbable-line > .nav-tabs > li {
  margin-right: 2px;
}
.tabbable-line > .nav-tabs > li > a {
  border: 0;
  margin-right: 0;
  color: #737373;
}
.tabbable-line > .nav-tabs > li > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
  border-bottom: 4px solid #fbcdcf;
}
.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
  border: 0;
  background: none !important;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
  margin-top: 0px;
}
.tabbable-line > .nav-tabs > li.active {
  border-bottom: 4px solid #16a085;
  position: relative;
}
.tabbable-line > .nav-tabs > li.active > a {
  border: 0;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.active > a > i {
  color: #404040;
}
.tabbable-line > .tab-content {
  margin-top: -3px;
  background-color: #fff;
  border: 0;
  border-top: 1px solid #eee;
  padding: 15px 0;
}
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}


.tgreen {
   color: #16a085;
   font-weight: bold;
}


</style>
@stop

@section('main_content')

<div class="row">
   <div class="col-md-12 mt1">
      <div class="profile-box">
    <h2 class="title-2"><i class="icon-user-add"></i> My activity list </h2>

         <div class="tabbable-panel">
            <div class="tabbable-line">
               <ul class="nav nav-tabs ">
                  <li class="active">
                     <a href="#tab_default_1" data-toggle="tab">
                    Selling </a>
                  </li>
                  <li>
                     <a href="#tab_default_2" data-toggle="tab">
                     Buying </a>
                  </li>
                </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_default_1">
             
                    <div class="row">
                      <div class="col-md-12">

                     <?php $i=1; ?>
                      @foreach($seller_order as $ord_s)
                      
                        <a href="{{ route('user.my-activity-seller',$ord_s->id) }}"><label class="tgreen">Order code #{{ $i }}</label></a> <sup> ( {{ date('d-M-Y', strtotime($ord_s->created_at)) }} ) </sup>

                          <table class="table table-bordered">
                            <tbody>
                              <tr>
                                <th style="width: 25%;">Status</th>
                                <td>
                                  @if($ord_s->order_current_status == 'order_placed')
                                  <a href="{{ route('user.my-activity-seller',$ord_s->id) }}"><label class="label label-warning">Pending</label></a></td>
                                  @else
                                  <a href="{{ route('user.my-activity-seller',$ord_s->id) }}"><label class="label label-success">Processing</label></a>
                                  @if($ord_s->seller_order_confirm == 0)
                                  <form class="delivered" action="{{ route('user.my-activity-order-delivered.post') }}" method="post">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="order_id" value="{{ $ord_s->id }}">
                                      <button type="submit" class="btn btn-danger btn-sm mt1">Order delivered</button>
                                  </form>
                                  @else
                                   <label class="label label-primary"><i class="fa fa-check"></i> Order delivered successfull</label>
                                  @endif
                                </td>
                                  @endif
                              </tr>
                              <tr>
                                <th style="width: 25%;">Buyer</th>
                                <td>{{ ucwords($ord_s->buyer->name) }}</td>
                              </tr>
                              <tr>
                                <th style="width: 25%;">Order time</th>
                                <td>{{ date('d-M-Y', strtotime($ord_s->order_date)) }}</td>
                              </tr>
                              <tr>
                                <th style="width: 25%;">Delivery</th>
                                <td>
                                  @if($ord_s->expected_delivery_date)
                                  <span>{{ date('d-M-Y', strtotime($ord_s->expected_delivery_date)) }} (Expected)</span>
                                  @else
                                  <span>---</span>
                                  @endif
                              </td>
                              </tr>
                              <tr>
                                <th style="width: 25%;">Bill amount</th>
                                <td><i class="fa fa-rupee"></i>&nbsp;{{ $ord_s->project_cost }}</td>
                              </tr>
                            </tbody>
                          </table>

                        <hr>

                        <?php $i++; ?>
                      @endforeach

                      </div>  

                    </div>
                   </div>


                  <div class="tab-pane" id="tab_default_2">
                     <div class="row">
                       <div class="col-md-12">
                          <?php $i=1; ?>
                      @foreach($buyer_order as $ord_b)
                      
                        <a href="{{ route('user.my-activity-buyer',$ord_b->id)}}"><label class="tgreen">Order code #{{ $i }}</label></a> <sup> ( {{ date('d-M-Y', strtotime($ord_b->created_at)) }} ) </sup>

                          <table class="table table-bordered">
                            <tbody>
                              <tr>
                                <th style="width: 25%;">Status</th>
                                <td>
                                
                                @if($ord_b->project_delivered   == 1)
                                <button class="btn btn-primary btn-sm"><i class="fa fa-check"></i> Successfully delivered</button>
                                @else

                                  @if($ord_b->order_current_status == 'order_placed')
                                  <a href="{{ route('user.my-activity-buyer',$ord_b->id)}}"><label class="label label-warning">Pending</label></a>
                                  @else

                                  <a href="{{ route('user.my-activity-buyer',$ord_b->id)}}"><label class="label label-success">Processing</label></a>

                                    @if($ord_b->seller_order_confirm == 1)
                                    <form action="{{ route('user.my-activity-order-received-delivered.post') }}" method="post" class="rcv_dlvrd">
                                      {{ csrf_field() }}
                                      <input type="hidden" name="order_buy_id" value="{{ $ord_b->id }}">
                                      <button type="submit" class="btn btn-danger btn-sm mt1">Received order</button>
                                    </form>
                                    @endif

                                  @endif

                                  @endif
                                </td>
                              </tr>

                              <tr>
                                <th style="width: 25%;">Seller</th>
                                <td>{{ ucwords($ord_b->seller->name) }}</td>
                              </tr>
                              <tr>
                                <th style="width: 25%;">Order time</th>
                                <td>{{ date('d-M-Y', strtotime($ord_b->order_date)) }}</td>
                              </tr>
                              <tr>
                                <th style="width: 25%;">Delivery</th>
                                <td>
                                  @if($ord_b->expected_delivery_date)
                                  <span>{{ date('d-M-Y', strtotime($ord_b->expected_delivery_date)) }} (Expected)</span>
                                  @else
                                  <span>---</span>
                                  @endif
                              </td>
                              </tr>
                              <tr>
                                <th style="width: 25%;">Bill amount</th>
                                <td><i class="fa fa-rupee"></i>&nbsp;{{ $ord_b->project_cost }}</td>
                              </tr>
                            </tbody>
                          </table>

                        <hr>

                        <?php $i++; ?>
                      @endforeach

                       </div>
                     </div>
                  </div>
                  
               </div>
            </div>
         </div>

      </div>
   </div> 
</div>  
@endsection



@section('pageJs')




<script>
 $(document).ready(function() {
   
  });
 $(".delivered").on("submit", function(){
        return confirm("Want to delivered order ?");
  });

 $(".rcv_dlvrd").on("submit", function(){
        return confirm("Want to received delivered order ?");
  });
</script>
@stop

