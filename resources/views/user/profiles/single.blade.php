@extends('ui.maiong_ui.main')

@section('pageCss')

<link rel="stylesheet" href="{!! asset('public-assets/plugins/owlcarousel/assets/owl.carousel.min.css') !!}">
<link rel="stylesheet" href="{!! asset('public-assets/plugins/owlcarousel/assets/owl.theme.default.min.css') !!}">

<style>
   .pic {
      height: 100px;
      width: 100px;
      border-radius: 50%;
      border: 1px solid #222;
   }
   .profile-box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
   }
   .mt1 {
      margin-top: 1em;
   }
   /* Tabs panel */
.tabbable-panel {
  border:1px solid #eee;
  padding: 10px;
}

/* Default mode */
.tabbable-line > .nav-tabs {
  border: none;
  margin: 0px;
}
.tabbable-line > .nav-tabs > li {
  margin-right: 2px;
}
.tabbable-line > .nav-tabs > li > a {
  border: 0;
  margin-right: 0;
  color: #737373;
}
.tabbable-line > .nav-tabs > li > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
  border-bottom: 4px solid #fbcdcf;
}
.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
  border: 0;
  background: none !important;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
  margin-top: 0px;
}
.tabbable-line > .nav-tabs > li.active {
  border-bottom: 4px solid #16a085;
  position: relative;
}
.tabbable-line > .nav-tabs > li.active > a {
  border: 0;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.active > a > i {
  color: #404040;
}
.tabbable-line > .tab-content {
  margin-top: -3px;
  background-color: #fff;
  border: 0;
  border-top: 1px solid #eee;
  padding: 15px 0;
}
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}
.nme {
   font-size: 18px;
   color: #222;
   font-weight: bold;
}
.cname {
   font-size: 16px;
   color: #16a085;
   font-weight: bold;
}
.prc {
   /*border: 1px solid #16a085;*/
   /*padding: .5em;
   border-radius: 5px;
   width: 80%;*/
}
.tgreen {
   color: #16a085;
   font-weight: bold;
}

.owl-carousel img {
  height: 250px !important;
  background-position: cover;
  -webkit-background-size: cover;
  background-size: cover;
}
</style>
@stop

@section('main_content')
<div class="row">
   <div class="col-md-12">
      
         <div class="row">

               <div class="col-md-8">
                  <div class="profile-box">
                     <div class="row">
                        <div class="col-md-6">
                           @foreach($img as $i)
                           <img src="{{ url('uploads/profiles/images/'.$i->image_path) }}" alt="Profile picture" class="pic">
                           @endforeach
                           <p class="nme mt1">{{ ucwords($sprofiles->user->name) }} </p>

                           
                           
                           <div class="prc">Package at &nbsp;<span class="tgreen"><i class="fa fa-rupee"></i> {{ round($sprofiles->package_amount) }}</span></div>
                        </div>

                        <div class="col-md-6">
                           <span class="cname"> {{ ucwords($sprofiles->profile_title) }}</span><br>
                           <div class="mt1">
                           <i class="fa fa-map-marker fa-2x"></i>&nbsp;&nbsp;

                           @foreach($location as $l)
                              <span>{{ $l->name }}</span><br>
                           @endforeach
                        </div>
                        <div class="mt1">
                           <i class="fa fa-calendar fa-2x"></i>&nbsp;&nbsp;
                           <span>Since {{ date('F  Y', strtotime($sprofiles->user->member_since)) }}</span>
                        </div>

                        <div class="mt1">
                           <i class="fa fa-bookmark fa-2x"></i>&nbsp;&nbsp;
                           <span>{{ ucwords($sprofiles->category->name) }}</span>
                        </div>

                        </div>

                  </div>
               </div> 
            </div>

               <div class="col-md-4">
                  <div class="profile-box">
                  <a href="{{ route('user.message.contact-me', $sprofiles->slug) }}" class="btn btn-success">Contact me</a>
                  <a href="{{ route('user.order-now', $sprofiles->slug) }}" class="btn btn-danger">Order now</a>
               </div>
            </div>

            </div>
         </div>
      

   <div class="col-md-12 mt1">
      <div class="profile-box">
         <div class="tabbable-panel">
            <div class="tabbable-line">
               <ul class="nav nav-tabs ">
                  <li class="active">
                     <a href="#tab_default_1" data-toggle="tab">
                    Overview </a>
                  </li>
                  <li>
                     <a href="#tab_default_2" data-toggle="tab">
                     Package </a>
                  </li>
                </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_default_1">
             
                    <div class="row">
                      <div class="col-md-3">
                        <div class="owl-carousel owl-theme">
                           @foreach($image as $img)
                               <img src="{{ url('uploads/profiles/images/'.$img->image_path) }}" alt="Profile pictures">
                           @endforeach
                        </div>
                      </div>

                      <div class="col-md-9">
                        <h3 class="tgreen"><strong>{{ ucwords($sprofiles->profile_title) }}</strong></h3>
                          <span>{{ ucwords($sprofiles->category->name) }}</span><br>
                        <hr>

                        <label>Overview</label><br>{!! $sprofiles->profile_description !!}
                      </div>
                    </div>
                    

                     
                  </div>
                  <div class="tab-pane" id="tab_default_2">
                     <div class="row">
                       <div class="col-md-12">
                          <h4>Standard package at <span class="tgreen"><i class="fa fa-rupee"></i> {{ round($sprofiles->package_amount) }}</span></h4>
                        <hr>

                        <label>Package description</label><br>{!! $sprofiles->package_description !!}
                       </div>
                     </div>
                  </div>
                  
               </div>
            </div>
         </div>

      </div>
   </div> 
</div>  
@endsection



@section('pageJs')

<script src="{!! asset('public-assets/plugins/owlcarousel/owl.carousel.js') !!}"></script>


<script>
 $(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
      items: 1,
      loop: true,
      margin: 10,
      autoplay: true,
      autoplayTimeout: 1500,
      autoplayHoverPause: true
    });
    $('.play').on('click', function() {
      owl.trigger('play.owl.autoplay', [1000])
    })
    $('.stop').on('click', function() {
      owl.trigger('stop.owl.autoplay')
    })
  })
</script>
@stop

