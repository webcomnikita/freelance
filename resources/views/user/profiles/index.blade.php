@extends('ui.maiong_ui.main')

@section('pageCss')
<style>
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  padding: .5em;
  margin: auto;
  text-align: center;
  
}

.mne {
   font-size: 12px;
   color: #222;
   font-weight: bold !important;
}
.title {
  color: #16a085;
  font-size: 18px;
  font-weight: bold;
}
.mb1 {
   margin-bottom: 1em;
}

.rbox {
   background-color: #16a085;
   padding: .6em;
   color: #fff;
}
</style>
@stop

@section('main_content')
<div class="row">
   @foreach($profiles as $p)
   <div class="col-md-4 mb1">
      
      <div class="card">  
        <span class="nme">{{ ucwords($p->user->name) }}</span>

        <p class="title">{{ ucwords($p->profile_title) }}</p>
        <p><a href="{{ route('category',$p->category->slug) }}">{{ ucwords($p->category->name) }}</a></p>



        <div style="margin: 24px 0;">
          <!-- <span class="nme">Strating at</span> -->
       </div>
       <a href="{{ route('profile.view', $p->slug) }}"><div class="rbox">Starting at <i class="fa fa-rupee"></i> {{ round($p->package_amount) }}</div></a>
      </div>
     
   </div>
    @endforeach
</div>   
@endsection



