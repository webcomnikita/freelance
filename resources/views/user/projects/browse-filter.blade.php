@extends('ui.maiong_ui.main')

@section('pageCss')
<style>
  .box {
    background-color: #fff;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    padding: 1em;
  }
</style>
@stop

@section('main_content')


<div class="row">
  @if(count($project_category_id) > 0)
  @foreach($project_category_id as $project)
   <div class="col-md-4">
      <div class="card card-1 mb1">
        <a href="{{ route('project.view',['category_name' => $project->category->name, 'slug' => $project->slug]) }}"><h3>{{ ucwords($project->name) }}</h3></a> <span class="red">{{ date('D, d M Y ', strtotime($project->created_at)) }}</span>

        <a href="{{ route('project.view',['category_name' => $project->category->name, 'slug' => $project->slug]) }}"><p>{!! Str::words($project->description, 5) !!}</p></a>

        <span><a href="{{ route('category',$project->category->slug) }}">{{ $project->category->name }}</a></span>
      </div>
    </div>
    @endforeach
  @else
  <div class="box">
      <h4>Selected category has no project</h4> 
  </div>
  @endif 
</div>
   
@endsection

