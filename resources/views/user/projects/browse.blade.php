@extends('ui.maiong_ui.main')

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <button class="btn btn-primary pull-right mb1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-filter"></i> Filter</button>
  </div>
  @foreach($projects as $project)
  
   <div class="col-md-4">
      <div class="card card-1 mb1">
        <a href="{{ route('project.view',['category_name' => $project->category->name, 'slug' => $project->slug]) }}"><h3>{{ ucwords($project->name) }}</h3></a> <span class="red">{{ date('D, d M Y ', strtotime($project->created_at)) }}</span>

        <a href="{{ route('project.view',['category_name' => $project->category->name, 'slug' => $project->slug]) }}"><p>{!! Str::words($project->description, 5) !!}</p></a>

        <span><a href="{{ route('category',$project->category->slug) }}">{{ $project->category->name }}</a></span>
      </div>
    </div>

   
    @endforeach
    <!-- <div class="col-md-4">
      <div class="card card-2">
        <h3>UI Components</h3>
        <p>Tabs, buttons, inputs, lists, cards, and more! A comprehensive library
          of mobile UI components, ready to go.</p>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-3">
        <h3>Theming</h3>
        <p>Learn how to easily customize and modify your app’s design to fit your
          brand across all mobile platform styles.</p>
      </div>
    </div> -->
</div>


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
   
      <!-- Modal content-->
      <div class="modal-content">
        <form action="{{ route('browse-jobs-by-filter') }}" method="post">
              {{ csrf_field() }}

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Filter projects by category</h4>
        </div>
        <div class="modal-body">
         
          <div class="row">
                <div class="col-md-12">
                  
                    <div class="form-group">
                      <div class="@if ($errors->has('category_id')) has-error @endif">
                        <label for="category_id">Select category</label>
                        <select class="form-control" name="category_id" id="category_id" required="required">
                          <option value="">-- Select your category --</option>
                          <?php foreach ($category as $c): ?>
                            <option value="{{ $c->id }}" {{ (old('category_id') == $c->id) ? 'selected' : '' }}>{{ $c->name }}</option>
                          <?php endforeach; ?>
                        </select>
                        @if ($errors->has('category_id'))
                        <p class="help-block">{{ $errors->first('category_id') }}</p>
                        @endif
                      </div>
                    </div>
                </div>
             </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Filter</button>
        </div>
      </form>

      </div>

    </div>
  </div>
   
@endsection

