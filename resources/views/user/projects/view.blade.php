@extends('ui.maiong_ui.main')

@section('main_content')
<div class="row">
   <div class="col-sm-9 page-content col-thin-right">
      <div class="inner inner-box ads-details-wrapper">
          <h2> {{ ucwords($project_data->name) }}</h2>

      <!--**********************************************************************-->
      <!--**********************************************************************-->
      @if(date('Y-m-d H:i:s') < $final) <!-- Start outer if 1-->
            @if($user_id != $project_user_id)
               @if($user_status == 1)   <!-- Profile Authorized or not-->
                  @if($haveBidding==false)
                    
                     @if($bidding_count <10 )
                      {{-- <a href="{{ route('contact-me-project',$project_data->slug) }}" class="btn btn-primary" style="margin-bottom: 1em;">Bid now1</a>  --}}
                      <a class="btn btn-danger mb1" href="{{ route('user.bidding.buy-premium') }}"><i class="fa fa-shopping-cart"></i> &nbsp; Buy premium version1</a>
                     @else

                        @if($isPremium==false)
                          @if($bidding_count <10 )
                               <a href="{{ route('contact-me-project',$project_data->slug) }}" class="btn btn-primary" style="margin-bottom: 1em;">Bid now3</a>
                           @else
                              <a class="btn btn-danger mb1" href="{{ route('user.bidding.buy-premium') }}"><i class="fa fa-shopping-cart"></i> &nbsp; Buy premium version</a>
                            @endif
                        @else
                          <a href="{{ route('contact-me-project',$project_data->slug) }}" class="btn btn-primary" style="margin-bottom: 1em;">Bid now2</a>
                         @endif
                     @endif

                  @else
                   <p>You have already bid on this project</p>
                  @endif
               @else
               <p>Your profile is not approved yet. Contact admin</p>
               @endif
            @else
               <p>you cannot bid for this project</p>
          @endif

      @else   <!-- Start outer if 1 else-->
         <p>Bidding Closed</p>
     @endif
      <!--**********************************************************************-->
      <!--**********************************************************************-->
         <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> Posted: {{ date('D, d M Y ', strtotime($project_data->created_at)) }} </span> </span>
         <div class="Ads-Details ">
            <div class="row">
               <div class="ads-details-info jobs-details-info col-md-8">
                  <p>{{ $project_data->description }}
                  </p>
               </div>
               <div class="col-md-4">
                  <aside class="panel panel-body panel-details job-summery">
                     <ul>
                        <li>
                           <p class=" no-margin "><strong>Category:</strong> {{ $project_data->category->name }} </p>
                        </li>
                        <li>
                           <p class=" no-margin "><strong>Budget:</strong> {{ $project_data->budget }} INR </p>
                        </li>
                        <!-- <li>
                           <p class=" no-margin "><strong>Contact Number:</strong> {{ $project_data->contact_number }} </p>
                        </li>-->
                        
                     </ul>
                  </aside>
               </div>
            </div>

       @if($user_id == $project_user_id)
            <div class="row">
              <div class="col-md-6">
                @if($project_data->disable == 1)
                <label>Project disable status</label>
                <span class="label label-danger">Disabled</span>
                @else
                <label>Project disable status</label>
                <span class="label label-success">Visible</span>
                @endif
              </div>

              <div class="col-md-6">
                <form action="{{ route('projects-disable-status.store') }}" method="post">
                  {{ csrf_field() }}
                  <input type="hidden" name="project_hidden_id" value="{{ $p_id }}">

                  <button type="submit" class="btn btn-danger mb1"><i class="fa fa-toggle-on"></i> Change disable status</button>
                </form>
              </div>
            </div>
        @endif


         </div>
      </div>
   </div>
   <div class="col-sm-3  page-sidebar-right">
      <aside>
         <div class="panel sidebar-panel panel-contact-seller">
            <div class="panel-heading">Related Projects</div>
            <div class="panel-content user-info">
               <div class="panel-body text-center">
                  <div class="seller-info">
                     <div class="company-logo-thumb">
                        <a><img alt="img" class="img-responsive img-circle" src="images/jobs/company-logos/17.jpg"> </a>
                     </div>
                     <h3 class="no-margin"> Data Systems Ltd</h3>
                     <p>Location: <strong>New York</strong></p>
                     <p> Web: <strong>www.demoweb.com</strong></p>
                  </div>
               </div>
            </div>
         </div>
      </aside>
   </div>

    <div class="col-sm-3  page-sidebar-right">
      <aside>
         <div class="panel sidebar-panel panel-contact-seller">
            <div class="panel-heading">Coupons for you</div>
            <div class="panel-content user-info">
               <div class="panel-body text-center">
                  <div class="seller-info">
                    @foreach($coupon as $c)
                     <p>{{ ucwords($c->coupon_name) }} : <strong><i class="fa fa-rupee"></i> {{ $c->coupon_discount_amount }}</strong></p>
                     @endforeach
                   </div>
               </div>
            </div>
         </div>
      </aside>
   </div>

</div>

   
@endsection

