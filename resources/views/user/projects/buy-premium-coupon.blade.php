@extends('ui.maiong_ui.main')

@section('pageCss')
<style>
.profile-box {
  background-color: #fff;
  padding: 1em;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.mt1 {
  margin-top: 1em;
}
</style>

@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="profile-box">
     <form id="applyCouponForm" name="applyCouponForm"  action="{{ route('user.bidding.buy-premium-coupon') }}"  method="post" >
        {{ csrf_field() }}
        
            <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY; ?>" />
            <input type="hidden" name="hash_string" value="<?php echo $hash_string; ?>" />
            <input type="hidden" name="hash" value="<?php echo $hash; ?>"  />
            <input type="hidden" name="txnid"  value="<?php echo $txnid; ?>" />
            <input name="surl"  type="hidden" value="{{ route('user.buy-premium.success') }}" />
            <input name="furl"  type="hidden"  value="{{ route('user.buy-premium.fail') }}"  />
            <input name="productinfo"  type="hidden" value="{{ $productinfo }}" />
            <input type="hidden" name="email" value="{{ $email }}">
            <input type="hidden" name="phone" value="{{ $mobile_no }}">
            <input type="hidden" name="service_provider" value="payu_paisa" />
            <input type="hidden" name="udf1" value="{{ $udf1 }}">
            <input type="hidden" name="udf2"  value="{{ $udf2 }}"> 
            <input type="hidden" name="udf3"  value="{{ $udf3 }}">
            <input type="hidden" name="udf4"  value="{{ $udf4 }}">  


            <input type="hidden" name="priceVal"    value="{{ $priceVal }}">
            <input type="hidden" name="amount"  id="amount"  value="{{ $priceVal }}"><br>



          <div class="panel panel-success">
            <div class="panel-heading"><b>Order Details </b></div>
                <div class="panel-body">
                  <div class="row">
             
                    <div class="col-md-12">
                        <span>Total Price :{{ $TotalVal }} </span><br>
                        <span>Discount Amount :{{ $discountVal }} </span><br>
                        <span>Total Amount :{{ $priceVal }} </span><br>
                    </div>
                    <div class="col-md-6">

                        <input type="text" name="coupon"  id="coupon" class="form-control" placeholder="Enter coupon if any"  value="{{ $mycoupon }}">
                    </div>

                    <div class="col-md-6">
                         <button type="submit"  class="btn btn-primary">Apply coupon</button>
                    </div>

                  </div> 

               </div>
            </div>
               
        </form>

             <!-- -->
             

        @if($priceVal>0)

             <form action="{{ route('user.bidding.buy-premium-confirm') }}" method="post">
              {{ csrf_field() }}

               <input type="text" name="aaa" value="{{ $TotalVal }}">
               <input type="text" name="bbb" value="{{ $discountVal }}">
               <input type="text" name="ccc" value="{{ $priceVal }}">

               <input name="productinfo"  type="text" value="{{ $productinfo }}" /><br>
                  <input type="text" name="service_provider" value="payu_paisa" /><br>

                  <input type="text" name="udf1"  id="udf1"  value="{{ $udf1 }}"><br>
                  <input type="text" name="udf2"  id="udf2"  value="{{ $udf2 }}"><br>
                  <input type="text" name="udf3"  id="udf3"  value="nill"><br>
                  <input type="text" name="udf4"  id="udf4"  value="{{ $udf4 }}"> <br>

                  <input type="text" name="priceVal"  value="{{ $priceVal }}"><br>
                  <input type="text" name="amount"  value="{{ $priceVal }}"><br>
                  <input type="text" name="name" value="{{ $name }}">
                  <input type="text" name="email" value="{{ $email }}">
                  <input type="text" name="phone" value="{{ $mobile_no }}">
                  <input type="text" name="address" value="{{ $location }}">
                  <input type="text" name="zipcode" value="{{ $zip_code }}">
                  
                  <input type="text" name="phone" value="{{ $mobile_no }}">

               <button type="submit" class="btn btn-primary btn-block">Proceed to pay</button>
             </form>
           
            
         @else  

          <div class="row">
             <div class="col-md-12">          

              <form  action="{{ route('user.bidding.coupon-discounted-applied') }}" method="post" >
               {{ csrf_field() }}
                  <input type="hidden" name="coupon_id"  value="{{ $mycoupon }}">

                  <input type="hidden" name="txnid"  value="<?php echo $txnid; ?>" />
                 

                  <input type="hidden" name="amount" value="{{ $priceVal }}"><br>
              

                  <button type="submit" class="btn btn-primary btn-block">Submit1</button>

                  </form>
              </div>
            </div>
               

            @endif
 
                               

    </div>
  </div>
</div>
  
@endsection


@section('pageJs')
<script>
 $(document).ready(function() {
    
  })

 $("#payuform").submit(function(e) {
    //alert('ddd');
    // e.preventDefault();
    $.ajax({
        type: "POST",
        url: "{{ url('user/message/buy-premium-init') }}",
        data: { 
            user_id: $('#user_id').val(), 
            package_amount: $('#package_amount').val(),
            _token: $('input[name="_token"]').val()
           },
        beforeSend : function(result){
            //alert('hii');
          },
        success: function(result) {
           $("#payuform").submit();
        },
        error: function(result) {
            console.log( "{{ url('user/message/buy-premium-init') }}"); 
            //alert(result.responseText);
        }

    });
    return true;
});
</script>
@stop

