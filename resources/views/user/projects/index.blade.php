@extends('ui.maiong_ui.main')

@section('main_content')

<div class="row">
  @foreach($projects as $project)
   <div class="col-md-4">
      <div class="card card-1 mb1">
        <a href="{{ route('project.view',['category_name' => $project->category->name, 'slug' => $project->slug]) }}"><h3>{{ ucwords($project->name) }}</h3></a> <span class="red">{{ date('D, d M Y ', strtotime($project->created_at)) }}</span>

        <a href="{{ route('project.view',['category_name' => $project->category->name, 'slug' => $project->slug]) }}"><p>{!! Str::words($project->description, 5) !!}</p></a>

        <span><a href="{{ route('category',$project->category->slug) }}">{{ $project->category->name }}</a></span>
      </div>
    </div>
    @endforeach
    <!-- <div class="col-md-4">
      <div class="card card-2">
        <h3>UI Components</h3>
        <p>Tabs, buttons, inputs, lists, cards, and more! A comprehensive library
          of mobile UI components, ready to go.</p>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-3">
        <h3>Theming</h3>
        <p>Learn how to easily customize and modify your app’s design to fit your
          brand across all mobile platform styles.</p>
      </div>
    </div> -->
</div>
   
@endsection

