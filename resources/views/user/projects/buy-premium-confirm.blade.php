@extends('ui.maiong_ui.main')

@section('pageCss')
<style>
.profile-box {
  background-color: #fff;
  padding: 1em;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.mt1 {
  margin-top: 1em;
}
</style>

@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="profile-box">

      <div class="panel panel-success">
            <div class="panel-heading"><b>Confirm Your Order Details </b></div>
                <div class="panel-body">
                  <div class="row">
             
                    <div class="col-md-12">
                        <span>Total Price :{{ $TotalVal }} </span><br>
                        <span>Discount Amount :{{ $discountVal }} </span><br>
                        <span>Total Amount :{{ $priceVal }} </span><br>
                    </div>
                
                  </div> 

               </div>
            </div>

    
              <form action="https://sandboxsecure.payu.in/_payment" id="payuform"  name="payuform" method="post" >
              {{ csrf_field() }}

              <div class="row">
                  <div class="col-md-12">
                  <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY; ?>" /><br>
                  <input type="hidden" name="hash_string" value="<?php echo $hash_string; ?>" /><br>
                  <input type="hidden" name="hash" value="<?php echo $hash; ?>"  /><br>
                  <input type="hidden" name="txnid"  value="<?php echo $txnid; ?>" /><br>
                  <input name="surl"  type="hidden" value="{{ route('user.buy-premium.success') }}" /><br>
                  <input name="furl"  type="hidden"  value="{{ route('user.buy-premium.fail') }}"  /><br>
                  <input name="productinfo"  type="hidden" value="{{ $productinfo }}" /><br>
                  <input type="hidden" name="service_provider" value="payu_paisa" /><br>

                  <input type="hidden" name="udf1"  id="udf1"  value="{{ $udf1 }}"><br>
                  <input type="hidden" name="udf2"  id="udf2"  value="{{ $udf2 }}"><br>
                  <input type="text" name="udf3"  id="udf3"  value="{{ $udf3 }}"><br>
                  <input type="hidden" name="udf4"  id="udf4"  value="{{ $udf4 }}"> <br>

                  <input type="hidden" name="priceVal"  value="{{ $priceVal }}"><br>
                  <input type="hidden" name="amount"  value="{{ $priceVal }}"><br>
              

                  <button type="submit" class="btn btn-primary btn-block">Proceed to pay</button>
                  </div>
                  </div>
              </form>  
           
                                   

    </div>
  </div>
</div>
  
@endsection


@section('pageJs')
<script>
 $(document).ready(function() {
    
  })

 $("#payuform").submit(function(e) {
    //alert('ddd');
    // e.preventDefault();
    $.ajax({
        type: "POST",
        url: "{{ url('user/message/buy-premium-init') }}",
        data: { 
            user_id: $('#user_id').val(), 
            package_amount: $('#package_amount').val(),
            _token: $('input[name="_token"]').val()
           },
        beforeSend : function(result){
            //alert('hii');
          },
        success: function(result) {
           $("#payuform").submit();
        },
        error: function(result) {
            console.log( "{{ url('user/message/buy-premium-init') }}"); 
            //alert(result.responseText);
        }

    });
    return true;
});
</script>
@stop

