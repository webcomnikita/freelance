@extends('ui.maiong_ui.main')

@section('pageCss')
<style type="text/css">
  /* Tabs panel */
.tabbable-panel {
  border:1px solid #eee;
  padding: 10px;
}

/* Default mode */
.tabbable-line > .nav-tabs {
  border: none;
  margin: 0px;
}
.tabbable-line > .nav-tabs > li {
  margin-right: 2px;
}
.tabbable-line > .nav-tabs > li > a {
  border: 0;
  margin-right: 0;
  color: #737373;
}
.tabbable-line > .nav-tabs > li > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
  border-bottom: 4px solid #fbcdcf;
}
.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
  border: 0;
  background: none !important;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
  margin-top: 0px;
}
.tabbable-line > .nav-tabs > li.active {
  border-bottom: 4px solid #16a085;
  position: relative;
}
.tabbable-line > .nav-tabs > li.active > a {
  border: 0;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.active > a > i {
  color: #404040;
}
.tabbable-line > .tab-content {
  margin-top: -3px;
  background-color: #fff;
  border: 0;
  border-top: 1px solid #eee;
  padding: 15px 0;
}
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}

.profile-box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
   }
   .mb1 {
    margin-bottom: 1em;
   }
   .font-sm {
    font-size: 10px;
   }
   .tgreen {
   color: #16a085;
   font-weight: bold;
}
</style>
@stop

@section('main_content')

  <div class="row">
   <div class="col-md-12">
      <div class="profile-box">
         <h2 class="title-2"><i class="icon-user-add"></i> My inbox </h2>
         <a href="{{ route('user.message.compose') }}" class="btn btn-success pull-right mb1"><i class="fa fa-envelope"></i> &nbsp; Create new</a>
         <div class="row">
            <div class="col-sm-12">
               
          <div class="tabbable-panel">
            <div class="tabbable-line">
               <ul class="nav nav-tabs ">
                  <li class="active">
                     <a href="#tab_default_1" data-toggle="tab">
                    Inbox </a>
                  </li>
                  <li>
                     <a href="#tab_default_2" data-toggle="tab">
                     Inquiry </a>
                  </li>
                  <li>
                     <a href="#tab_default_3" data-toggle="tab">
                     Sent </a>
                  </li>
                </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_default_1">
             
                    <div class="row">
                      <div class="col-md-12">
                        @foreach($my_inbox as $inbox)
                        <a href="{{ route('user.message.my-inbox-details',$inbox->id ) }}">
                          <span><strong>{{ ucwords($inbox->sender->name) }}</strong></span></a><br>
                          <span>Subject : {{ $inbox->subject }}</span>
                          <span class="tgreen font-sm pull-right">at {{ date('Y-M-d', strtotime($inbox->message_date)) }}</span>
                          <br>
                          Message : {!! $inbox->message !!}<br>
                          
                          <hr>
                        @endforeach
                      </div>
                    </div>
                      
                  </div>
                  <div class="tab-pane" id="tab_default_2">
                     <div class="row">
                       <div class="col-md-12">
                          @foreach($inqry as $inq)
                          <a href="{{ route('user.message.my-inquiry-details', $inq->id) }}">
                          <span><strong>{{ ucwords($inq->sender->name) }}</strong></span></a>
                          <span class="tgreen font-sm pull-right">at {{ date('Y-M-d', strtotime($inq->created_at)) }}</span>
                          <br>
                          Message : {!! $inq->message !!}<br>
                          
                          <hr>

                        @endforeach
                       </div>
                     </div>
                  </div>

                  <div class="tab-pane" id="tab_default_3">
                    <div class="row">
                      <div class="col-md-12">
                        @foreach($sent as $snt)
                        <a href="{{ route('user.message.my-sent-details', $snt->id) }}">
                          <span><strong>{{ ucwords($snt->sender->name) }}</strong></span></a><br>
                          <span>Subject : {{ $snt->subject }}</span>
                          <span class="tgreen font-sm pull-right">at {{ date('Y-M-d', strtotime($snt->message_date)) }}</span>
                          <br>
                           Message : {!! $snt->message !!}<br>
                          
                          <hr>
                        @endforeach
                    </div>
                   </div>
                </div>


            </div>
         </div>

            </div>
         </div>
      </div>
   </div>
</div>
</div>

@endsection

@section('pageJs')
@stop
