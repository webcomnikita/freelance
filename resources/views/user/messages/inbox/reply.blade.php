@extends('ui.maiong_ui.main')

@section('pageCss')
<link href="{{ asset('public-assets/plugins/selectize.js/dist/css/selectize.css') }}" rel="stylesheet">
<style type="text/css">

.profile-box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
   }
   .mb1 {
    margin-bottom: 1em;
   }
   .font-sm {
    font-size: 10px;
   }
   .tgreen {
   color: #16a085;
   font-weight: bold;
}
.mt1 {
  margin-top: 1em;
}

</style>
@stop

@section('main_content')
<div class="row">
   <div class="col-md-12 page-content">
      <div class="profile-box">
         <h2 class="title-2"><i class="icon-user-add"></i> Reply for message {{ $sub }} to {{ $sndr_name }} </h2>
         
         <div class="row">
            
              <form action="{{ route('user.message.my-inbox-reply.post') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="reply_to" value="{{ $msg_id }}">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="receiver_id" class="control-label">To</label>
                    
                       <input type="hidden" name="receiver_id" value="{{ $sndr_id }}" >

                       <input type="text" value="{{ $sndr_name }}" class="form-control"> 
                    </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="subject" class="control-label">Subject</label>
                    
                       <input type="text" name="subject" class="form-control"> 
                    </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="subject" class="control-label">Message</label>
                    
                       <textarea name="message" id="message" class="form-control"></textarea>
                    </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group lastfileinput">
                    <label for="subject" class="control-label">Files</label>
                    
                       <input type="file" name="files[]" id="files"> 
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-md-4 mt1"> <a href="javascript:void(0)" id="add_file_input">ADD </a></div>
                </div>


     
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-block mt1" >Send Message</button>
              </div>
              
            </form>  

            
          </div>
        </div>
      </div>
    </div>


@endsection

@section('pageJs')
<script type="text/javascript" src="{{ asset('public-assets/plugins/selectize.js/dist/js/standalone/selectize.min.js') }}"></script>
 
 <script>

  $('#select-repo').selectize({
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    maxItems: null,
    create: false,
    render: {
     option: function(item, escape) {
            return '<div>' +
                '<span class="">' +
                    '<span class="">' + escape(item.name) + '</span>' + '' +
                    '<span class="tgreen"> ( ' + escape(item.email) + ' )</span>' +
                '</span>' +
                
            '</div>';
           
         console.log("init Val => "+init);
        }
    },
   
    load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '{{ url('/api/user-list1') }}?' + encodeURIComponent(query),
            type: 'GET',
            contentType: "json",
            error: function() {
                console.log('error'+ '{{ url('/api/user-list1') }}?' + encodeURIComponent(query));
                callback();
            },
            success: function(res) {
                console.log(res);
                callback(res.repositories.slice(0, 5));
            }
        });
    }
});

$('#add_file_input').click(function() {
   $lastFileInput = $('.lastfileinput:last');
   $clone = $lastFileInput.clone();
   $lastFileInput.after($clone);
});

</script>
@stop
