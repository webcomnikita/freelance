@extends('ui.maiong_ui.main')

@section('pageCss')

<link href="{{ asset('public-assets/plugins/selectize.js/dist/css/selectize.css') }}" rel="stylesheet">


@stop

@section('main_content')
<div class="row">
   <div class="col-md-12 page-content">
      <div class="inner-box category-content">
         <h2 class="title-2"><i class="icon-user-add"></i> Create New Message </h2>
         <div class="row">
            <div class="col-sm-12">
                @if ($errors->any())
                  {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
                @endif

               {!! Form::open(array('route' => 'user.message.send', 'method' => 'post', 'id' => 'user.message.send', 'class' => 'form-horizontal row-border','files' => true)) !!}
                  <fieldset>

                     @include('user.messages._create')

                     <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-8">
                           <div style="clear:both"></div>
                           <button type="submit" class="btn btn-primary" >Send Message</button>
                        </div>
                     </div>
                  </fieldset>
               {!! Form::close() !!}

            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('pageJs')
<script type="text/javascript" src="{{ asset('public-assets/plugins/selectize.js/dist/js/standalone/selectize.min.js') }}"></script>
 
 <script>

  $('#select-repo').selectize({
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    maxItems: null,
    create: false,
    render: {
     option: function(item, escape) {
            return '<div>' +
                '<span class="">' +
                    '<span class="">' + escape(item.name) + '</span>' + '' +
                    '<span class="tgreen"> ( ' + escape(item.email) + ' )</span>' +
                '</span>' +
                
            '</div>';
           
         console.log("init Val => "+init);
        }
    },
   
    load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '{{ url('/api/user-list1') }}?' + encodeURIComponent(query),
            type: 'GET',
            contentType: "json",
            error: function() {
                console.log('error'+ '{{ url('/api/user-list1') }}?' + encodeURIComponent(query));
                callback();
            },
            success: function(res) {
                console.log(res);
                callback(res.repositories.slice(0, 5));
            }
        });
    }
});

$('#add_file_input').click(function() {
   $lastFileInput = $('.lastfileinput:last');
   $clone = $lastFileInput.clone();
   $lastFileInput.after($clone);
});

</script>


@stop

