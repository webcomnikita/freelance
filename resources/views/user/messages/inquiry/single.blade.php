@extends('ui.maiong_ui.main')

@section('pageCss')
<style type="text/css">

.profile-box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
   }
   .mb1 {
    margin-bottom: 1em;
   }
   .font-sm {
    font-size: 10px;
   }
   .tgreen {
   color: #16a085;
   font-weight: bold;
}

.tabbable-panel {
  border:1px solid #eee;
  padding: 10px;
}

.doc-view {
  border: 1px solid #16a085;
  padding: .6em;
}
</style>
@stop

@section('main_content')

  <div class="row">
   <div class="col-md-12">
      <div class="profile-box">
         <h2 class="title-2"><i class="icon-user-add"></i> Inquiry details for <span class="tgreen">{{ ucwords($inq_id->sender->name) }}</span> </h2>

         <a class="btn btn-border btn-danger" href=""><i class="fa fa-reply"></i> &nbsp; Reply</a>

         <a href="{{ route('user.message.compose') }}" class="btn btn-success pull-right mb1"><i class="fa fa-envelope"></i> &nbsp; Create new</a>
         <div class="row">
            <div class="col-sm-12">
               
              <div class="tabbable-panel">
                <span><strong>{{ ucwords($inq_id->sender->name) }}</strong></span></a><br>
                
                  <span class="tgreen font-sm pull-right">at {{ date('Y-M-d', strtotime($inq_id->message_date)) }}</span>
                  <br>
                  Message : {!! $inq_id->message !!}<br>
                  
                  <hr>

                  
                  @if($i_attch)
                  <div class="row">
                    <div class="col-md-12">
                      <h3><i class="fa fa-paperclip fa-2x tgreen"></i>You Have {{ $i_attch_count }} Attatchment</h3>
                    </div>
                    <?php $i=1; ?>
                    @foreach($i_attch as $attch)
                    
                    <div class="col-md-3">
                      <div class="doc-view">
                        <a target="_blank" href="{{ url('/uploads/profiles/inquiries/'.$attch->file_path) }}">View doc {{ $i }}</a>
                      </div>
                    </div>
                     <?php $i++; ?>
                    @endforeach
                  </div>
                @else
                <p>You have no attatchment</p>
                @endif
               </div>

            </div>
         </div>
      </div>
   </div>
</div>
</div>

@endsection

@section('pageJs')
@stop
