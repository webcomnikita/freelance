@extends('ui.maiong_ui.main')

@section('pageCss')

@stop

@section('main_content')

<div class="row">
   <div class="col-md-12 page-content">
      <div class="inner-box category-content">
         <h2 class="title-2"><i class="icon-user-add"></i> Tell me about </h2>
         <div class="row">
            <div class="col-sm-12">
                @if ($errors->any())
                  {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
                @endif

               {!! Form::open(array('route' => 'user.message.contact-me.sent', 'method' => 'post', 'id' => 'user.message.send', 'class' => 'form-horizontal row-border','files' => true)) !!}
                  <fieldset>

                     @include('user.messages._contact-me')

                     <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-8">
                           <div style="clear:both"></div>
                           <button type="submit" class="btn btn-primary" >Send Message</button>
                        </div>
                     </div>
                  </fieldset>
               {!! Form::close() !!}

            </div>
         </div>
      </div>
   </div>
</div>  

@endsection

@section('pageJs')

<script>

$('#add_file_input').click(function() {
   $lastFileInput = $('.lastfileinput:last');
   $clone = $lastFileInput.clone();
   $lastFileInput.after($clone);
});

</script>
@stop
