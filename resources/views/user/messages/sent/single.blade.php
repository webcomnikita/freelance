@extends('ui.maiong_ui.main')

@section('pageCss')
<style type="text/css">

.profile-box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
   }
   .mb1 {
    margin-bottom: 1em;
   }
   .font-sm {
    font-size: 10px;
   }
   .tgreen {
   color: #16a085;
   font-weight: bold;
}

.tabbable-panel {
  border:1px solid #eee;
  padding: 10px;
}

.doc-view {
  border: 1px solid #16a085;
  padding: .6em;
}
</style>
@stop

@section('main_content')

  <div class="row">
   <div class="col-md-12">
      <div class="profile-box">
         <h2 class="title-2"><i class="icon-user-add"></i> {{ $msg_id->subject }} details </h2>
         <a href="{{ route('user.message.compose') }}" class="btn btn-success pull-right mb1"><i class="fa fa-envelope"></i> &nbsp; Create new</a>
         <div class="row">
            <div class="col-sm-12">
               
              <div class="tabbable-panel">
                <span><strong>{{ ucwords($msg_id->sender->name) }}</strong></span></a><br>
                <span>Subject : {{ $msg_id->subject }}</span>
                  <span class="tgreen font-sm pull-right">at {{ date('Y-M-d', strtotime($msg_id->message_date)) }}</span>
                  <br>
                  Message : {!! $msg_id->message !!}<br>
                  
                  <hr>

                  
                  @if($m_attch)
                  <div class="row">
                    <div class="col-md-12">
                      <h3><i class="fa fa-paperclip fa-2x tgreen"></i>You Have {{ $m_attch_count }} Attatchment</h3>
                    </div>
                    <?php $i=1; ?>
                    @foreach($m_attch as $attch)
                    
                    <div class="col-md-3">
                      <div class="doc-view">
                        <a href="{{ url('/uploads/profiles/messages/'.$attch->file_path) }}">View doc {{ $i }}</a>
                      </div>
                    </div>
                     <?php $i++; ?>
                    @endforeach
                  </div>
                @else
                <p>You have no attatchment</p>
                @endif
               </div>

            </div>
         </div>
      </div>
   </div>
</div>
</div>

@endsection

@section('pageJs')
@stop
