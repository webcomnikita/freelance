

<div class="form-group required  {{ $errors->has('message') ? 'has-error' : ''}}">
   <label for="inputEmail3" class="col-md-4 control-label">Project desceiption
   <sup>*</sup></label>
   <div class="col-md-6">
      {!! Form::textarea('message', null, ['class' => 'form-control required', 'id' => 'message', 'placeholder' => 'Your Message', 'rows' => 7, 'autocomplete' => 'off', 'required' => 'true']) !!}
   </div>
   {!! $errors->first('message', '<span class="help-inline">:message</span>') !!}
</div>

<input type="hidden" name="profile_id" value="{{ $sprofiles }}">


<div class="form-group lastfileinput">
   <label class="col-md-4 control-label">Attatchment <sup>{optional)</sup></label>
   <div class="col-md-6 ">
    

      {!! Form::file('files[]', null, ['class' => 'form-control', 'id' => 'files']) !!}
   </div>
</div>

<div class="form-group ">
   <label class="col-md-4 control-label"> </label>
   

   <div class="col-md-6"> <a href="javascript:void(0)" id="add_file_input">ADD </a></div>
</div>

