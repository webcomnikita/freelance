@extends('ui.maiong_ui.main')

@section('intro')
<div class="intro jobs-intro hasOverly" style="background-image: url( {{ asset('public-assets/bg.jpg') }} ); background-position: center center;">
   <div class="dtable hw100">
      <div class="dtable-cell hw100">
         <div class="container text-center">
            <h1 class="intro-title animated fadeInDown"> Find the Right Job </h1>
            <p class="sub animateme fittext3 animated fadeIn"> Find the latest jobs available in your area. </p>
            <div class="row search-row animated fadeInUp">
               <div class="col-lg-4 col-sm-4 search-col relative locationicon">
                  <i class="icon-location-2 icon-append"></i>
                  <input type="text" name="country" id="autocomplete-ajax" class="form-control locinput input-rel searchtag-input has-icon" placeholder="city, state, or zip" value="">
               </div>
               <div class="col-lg-4 col-sm-4 search-col relative"><i class="icon-docs icon-append"></i>
                  <input type="text" name="ads" class="form-control has-icon" placeholder="job title, keywords or company" value="">
               </div>
               <div class="col-lg-4 col-sm-4 search-col">
                  <button class="btn btn-primary btn-search btn-block"><i class="icon-search"></i><strong>Find
                  Jobs</strong></button>
               </div>
            </div>
            <div class="resume-up">
               <a><i class="icon-doc-4"></i></a> <a><b>Upload your CV</b></a> and easily apply to jobs from any
               device!
            </div>
         </div>
      </div>
   </div>
</div>
@stop

@section('main_content')
<div class="col-lg-12 content-box ">
   <div class="row row-featured row-featured-category row-featured-company">
      <div class="col-lg-12  box-title no-border">
         <div class="inner">
            <h2><span>Featured</span>
               Categories <!-- <a class="sell-your-item" href="job-list.html"> See all companies <i class="  icon-th-list"></i> </a> -->
            </h2>
         </div>
      </div>
      @foreach($categories as $ck => $cv)
      <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category">
         <a href="{{ route('public.view_jobs.categories', $cv->slug) }}">
            <img alt="img" class="img-responsive" src="{{ asset($cv->icon_path) }}">
            <h6> <span class="company-name">{{ $cv->name }}</span> </h6>
         </a>
      </div>
      @endforeach
      
   </div>
</div>
<div style="clear: both"></div>
@endsection

@section('hiw')

<!-- how it works -->
<div class="bg-black">
   <h2 class="text-center text">How It Works</h2>

   <div class="row">
      <div class="col-md-3">
         <div class="box">
            <article class="text-block">
              <header>
                  <img src="{!!asset('public-assets/images/icons/1.png') !!}" class="hiw-icon" alt="">
                  <hr>              
               </header>
              <!--  <h3 class="text-uppercase">FIND</h3> -->
               <span class="text-short text-center hiwtext">Client posts a project, selects city to hire freelancers. </span>            
            </article>
         </div>
      </div>

      <div class="col-md-3">
         <div class="box">
            <article class="text-block">
              <header>
                  <img src="{!!asset('public-assets/images/icons/2_A.png') !!}" class="hiw-icon" alt="">
                  <hr>              
               </header>
               <!-- <h3 class="text-uppercase">HIRE</h3> -->
               <span class="text-short text-center hiwtext">Freelancers from selected area gets notified, interested ones place their proposals.</span>
            </article>
         </div>
      </div>

      <div class="col-md-3">
         <div class="box">
            <article class="text-block">
              <header>
                  <img src="{!!asset('public-assets/images/icons/3.png') !!}" class="hiw-icon" alt="">
                  <hr>              
               </header>
               <!-- <h3 class="text-uppercase">WORK</h3> -->
               <span class="text-short text-center hiwtext">Client selects freelancer, both agree to the terms.</span>            
            </article>
         </div>
      </div>

      <div class="col-md-3">
         <div class="box">
            <article class="text-block">
              <header>
                  <img src="{!!asset('public-assets/images/icons/4.png') !!}" class="hiw-icon" alt="">
                  <hr>              
               </header>
               <!-- <h3 class="text-uppercase">PAY</h3> -->
               <span class="text-short text-center hiwtext">After agreed to the project terms proceed your project inside or outside the portal.</span>            
            </article>
         </div>
      </div>

   </div>
</div>
<!-- end of how it works -->


<!-- Why people use our secure payment -->

<div class="bg-green">
   <h2 class="text-center text">Our Secure Pick System</h2>

   <div class="row">
      <div class="col-md-3">
         <img src="{!!asset('public-assets/images/icons/A1.png') !!}" alt="image" class="img-responsive aimage">

         <div class="oouter">
            <div class="iinner"></div>
         </div>

         <div class="btmbox">
            After dealing with the freelancer, client deposits the fund in Secure Pick System.
         </div>
      </div>

      <div class="col-md-3">
         <img src="{!!asset('public-assets/images/icons/A2.png') !!}" alt="image" class="img-responsive aimage">

         <div class="oouter">
            <div class="iinner"></div>
         </div>

         <div class="btmbox">
            After the delivery of work, client releases the fund and it goes directly to the freelancer's account.
         </div>
      </div>

      <div class="col-md-3">
         <img src="{!!asset('public-assets/images/icons/A3.png') !!}" alt="image" class="img-responsive aimage">

         <div class="oouter">
            <div class="iinner"></div>
         </div>

         <div class="btmbox">
            If the freelancer doesnot deliver the work, the client gets the money back.
         </div>
      </div>

      <div class="col-md-3">
         <img src="{!!asset('public-assets/images/icons/A4.png') !!}" alt="image" class="img-responsive aimage">

         <div class="oouter">
            <div class="iinner"></div>
         </div>

         <div class="btmbox">
            The safest way to ensure the project.
         </div>
      </div>
   </div>
</div>

<!-- end of Why people use our secure payment -->

@endsection


