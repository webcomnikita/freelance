@extends('ui.maiong_ui.main')

@section('pageCss')
<style>
.box {
  background-color: #fff;
  padding: 1em;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.mt1 {
  margin-top: 1em;
}
.mt2 {
  margin-top: 2em;
}
.ml2 {
  margin-left: 2em;
}
h3 {
  color: #16A085;
}
h4 {
  color: #16A085;
}
.heading4 {
  color: #16A085;
  font-size: 17px;
  line-height: 20px;
}
</style>
@stop

@section('main_content')

<div class="row">
  <div class="col-md-6">
    <div class="box">
      <h3>Fees And Charges</h3>


      <ul class="">
        <li><i class="fa fa-arrow-right"></i> &nbsp; The following fees are currently charged for our services. <br>
          <b>The fees many be changed at any period of time according to the decision of Pick My Project.</b><br>
        </li>
      </ul>

      <hr>

      <h3>Withdrawal Fees</h3>

      <p>Fees may be optionally levied depending on the method of withdrawal. Additional fees may be levied by the third party offering the withdrawal method. </p>


      <h3>Bid Fees</h3>

      <table class="table table-responsive">
        <tr>
          <td>Bidding on projects</td>
          <td>10 FREE bids per month. After 10 bids, get unlimited bids per month by purchasing a package.</td>
        </tr>
      </table>


    </div>
  </div>


  <div class="col-md-6">
    <div class="box">
      <h3>Projects</h3>


      <table class="table table-responsive">
        <tr>
          <td>Posting a Project</td>
          <td>FREE</td>
        </tr>
        <tr>
          <td>Featured</td>
          <td>FREE</td>
        </tr>
        <tr>
          <td>Urgent</td>
          <td>FREE</td>
        </tr>
        <tr>
          <td>Private </td>
          <td>FREE</td>
        </tr>
        <tr>
          <td>Recruiter </td>
          <td>FREE</td>
        </tr>
        <tr>
          <td>Priority </td>
          <td>FREE</td>
        </tr>

      </table>



    </div>
  </div>
</div>



@endsection

