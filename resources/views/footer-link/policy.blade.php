@extends('ui.maiong_ui.main')

@section('pageCss')
  <style>
    .box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    .mt1 {
      margin-top: 1em;
    }
    .mt2 {
      margin-top: 2em;
    }
    h3 {
      color: #16A085;
    }
    h4 {
      color: #16A085;
    }
  </style>
@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <h3>Privacy Policy</h3>
      
        <p>Pick My Project ("us", "we", or "our") operates the http://www.pickmyproject.com/ website (the "Service").</p>
        <p>This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.</p>
        <p>We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible from http://www.pickmyproject.com.</p>

        <h4>Information Collection And Use</h4>
        <p>We collect several different types of information for various purposes to provide and improve our Service to you.</p>

        <h4>Types of Data Collected</h4>
        <p><b>Personal data</b></p>

        <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you ("Personal Data"). Personally identifiable information may include, but is not limited to:</p> 

        <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; Email address.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; First name and last name.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Phone number.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Address, State, Province, ZIP/Postal code, City.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Cookies and Usage Data.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Usage Data.</li>
       </ul>

       <p>We may also collect information how the Service is accessed and used ("Usage Data"). This Usage Data may include information such as your computer's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>

      <h4>Tracking & Cookies Data</h4>
       <p>We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.</p>

       <p>Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.</p>

       <p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.</p>

       <p>Examples of Cookies we use:</p>

       <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; Session Cookies. We use Session Cookies to operate our Service.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Preference Cookies. We use Preference Cookies to remember your preferences and various settings.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Security Cookies. We use Security Cookies for security purposes.</li>
       </ul>

       <h4>Use of Data</h4>
       <p>Pick My Project uses the collected data for various purposes: </p>

        <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; To provide and maintain the Service.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To notify you about changes to our Service.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To allow you to participate in interactive features of our Service when you choose to do so.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To provide customer care and support.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To provide analysis or valuable information so that we can improve the Service.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To monitor the usage of the Service.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To detect, prevent and address technical issues.</li>
       </ul>


       <h4>Transfer Of Data</h4>
       <p>Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>

       <p>If you are located outside India and choose to provide information to us, please note that we transfer the data, including Personal Data, to India and process it there.</p>

       <p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>

       <p>Pick My Project will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>

      <h4>Disclosure Of Data</h4>
      <p><b>Legal Requirements</b></p>
      <p>Pick My Project may disclose your Personal Data in the good faith belief that such action is necessary to:</p>

      <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; To comply with a legal obligation.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To protect and defend the rights or property of Pick My Project.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To prevent or investigate possible wrongdoing in connection with the Service.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To protect the personal safety of users of the Service or the public.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To protect against legal liability.</li>
      </ul>


      <h4>Security Of Data</h4>
      <p>The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.</p>

      <h4>Service Providers</h4>
      <p>We may employ third party companies and individuals to facilitate our Service ("Service Providers"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>
      <p>These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>

      <h4>Analytics</h4>
      <p>We may use third-party Service Providers to monitor and analyze the use of our Service.</p>

      <p><b>Google Analytics</b></p>

      <p>Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses the data collected to track and monitor the use of our Service. This data is shared with other Google services. Google may use the collected data to contextualize and personalize the ads of its own advertising network.</p>

      <p>You can opt-out of having made your activity on the Service available to Google Analytics by installing the Google Analytics opt-out browser add-on. The add-on prevents the Google Analytics JavaScript (ga.js, analytics.js, and dc.js) from sharing information with Google Analytics about visits activity.</p>

      <h4>Links To Other Sites</h4>
      <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>

      <p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>

      <h4>Changes To This Privacy Policy</h4>
      <p>We may update our Privacy Policy from time to time. We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the "effective date" at the top of this Privacy Policy.</p>

      <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>

     
        </div>
      </div>

  

</div>
   
@endsection

