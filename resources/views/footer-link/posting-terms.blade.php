@extends('ui.maiong_ui.main')

@section('pageCss')
  <style>
    .box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    .mt1 {
      margin-top: 1em;
    }
    .mt2 {
      margin-top: 2em;
    }
    h3 {
      color: #16A085;
    }
  </style>
@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <h3>Posting Terms</h3>
      
        <p>All users should read and accept the following terms in order to post a project on Pick My Project.</p>

       <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; Must Sign up in order to post a project.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Must not violate any Privacy Policy of this platform.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Must not use project posting as testing for any experiment.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Must not use any inappropriate content on the project.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Must not talk about any political, religious and cast related topics on the project.</li>
       </ul>

       <p>On the violation of the above terms, the user profile may be terminated immediately and may never be recovered.</p>

     
        </div>
      </div>

  

</div>
   
@endsection

