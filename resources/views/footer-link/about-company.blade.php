@extends('ui.maiong_ui.main')

@section('pageCss')
  <style>
    .box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    .mt1 {
      margin-top: 1em;
    }
    .mt2 {
      margin-top: 2em;
    }
    h3 {
      color: #16A085;
    }
  </style>
@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <h3>About Company</h3>
        <p>Pick My Project is a startup company established and started formerly from Guwahati and looking forward to extending our services globally as we grow. Keeping in view to providing a platform to the huge number of  skilled people around and those not getting proper value for their work, we are starting this platform to solve this problem and create an opportunity for everyone out there irrespective of any degree or qualifications and focus on talent of the person. </p>
        </div>
      </div>

    <div class="col-md-6 mt2">
        <div class="box">
          <div class="row">
            <div class="col-md-6">
              <h3>Address</h3>
              <p>Guwahati - 781034, Assam, India</p>
            </div>
            <div class="col-md-6">
              <h3>Support</h3>
              <p>info@pickmyproject.com</p>
            </div>
            <div class="col-md-12">
              <h3>Vision</h3>
              <p>To provide quality services to everyone registered with Pick My Project.</p>
              <h3>Mission</h3>
              <p>To provide beneficial services to the clients and freelancers through our user-friendly easy-to-use platform.</p>
            </div>
          </div>
        </div>
    </div>

    <div class="col-md-6 mt2">
      <div class="box">
        <div class="row">

          <div class="col-md-12">
            <h3>Goals</h3>
            <ul class="list-unstyled">
              <li><i class="fa fa-hand-point-right"></i> Regional expansion.</li>
              <li>Investments in the company for further development and expansion of our services.</li>
              <li>To grow Pick My Project as a reputed and helpful platform globally.</li>
            </ul>
          </div>

         <div class="col-md-12 mt1"> 
            <h3>Our Purpose</h3>
            <p>To provide a platform to the abundant skilled people to showcase their skills and get paid for whatever they are good at.</p>
          </div>
        </div>
      </div>
    </div>


</div>
   
@endsection

