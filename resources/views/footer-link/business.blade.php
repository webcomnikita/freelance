@extends('ui.maiong_ui.main')

@section('pageCss')
  <style>
    .box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    .mt1 {
      margin-top: 1em;
    }
    .mt2 {
      margin-top: 2em;
    }
    h3 {
      color: #16A085;
    }
  </style>
@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <h3>Business</h3>
        <p>If you have any inquiry for business opportunities, then please contact our support team at info@pickmyproject.com. </p>

        {{-- <h3>Purpose</h3>
        <p>To provide a platform to the abundant skilled people to showcase their skills and get paid for whatever they are good at. </p>

        <h3>Vision</h3>
        <p>To provide quality services to everyone registered with Pick My Project. </p>

        <h3>Mission</h3>
        <p>To provide beneficial services to the clients and freelancers through our user-friendly easy-to-use platform. </p>

        <h3>Goals</h3>
        <ul class="list-unstyled">
          <li>Regional expansion.</li>
          <li>Investments in the company for further development and expansion of our services.</li>
          <li>To grow Pick My Project as a reputed and helpful platform globally.</li>
        </ul> --}}

        </div>
      </div>

  

</div>
   
@endsection

