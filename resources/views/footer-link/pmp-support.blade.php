@extends('ui.maiong_ui.main')

@section('pageCss')
  <style>
    .box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    .mt1 {
      margin-top: 1em;
    }
    .mt2 {
      margin-top: 2em;
    }
    h3 {
      color: #16A085;
    }
  </style>
@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <h3>PMP Support</h3>
      
        <p>Pick My Project is a startup company established and started formerly from Guwahati and looking forward to extending our services globally as we grow. Keeping in view to providing a platform to the huge number of  skilled people around and those not getting proper value for their work, we are starting this platform to solve this problem and create an opportunity for everyone out there irrespective of any degree or qualifications and focus on talent of the person.</p>

        <p>Our customer support is always happy and ready to help you and resolve your queries. If you have any queries, then you can reach us by sending a mail at info@pickmyproject.com.</p>

        <p>Please describe your query in details and if you have any documents such as screenshots, then attach along with the mail.</p>

        </div>
      </div>

  

</div>
   
@endsection

