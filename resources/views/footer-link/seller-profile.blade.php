@extends('ui.maiong_ui.main')

@section('pageCss')
  <style>
    .box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    .mt1 {
      margin-top: 1em;
    }
    .mt2 {
      margin-top: 2em;
    }
    h3 {
      color: #16A085;
    }
  </style>
@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <h3>How To Create A Seller Profile</h3>
      
        <p>It is easy to create a seller profile in Pick My Project with the following simple steps:</p>

       <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; If you are new to Pick My Project, then click on Sign Up.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Enter the required details in the form and click on Register.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; After registering your account successfully, click on the button on top right corner with your name.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Select Seller Account.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; To create a Seller Account, fill up the form carefully with full details.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Click on Create Profile.</li>
       </ul>

       <p>After creating a seller profile, wait for your profile to be approved by Pick My Project. It may take upto 72 hours for your profile to be approved or rejected.</p>

       <p>A user can create many seller profiles but can active a maximum of 5 seller profiles.</p>

        </div>
      </div>

  

</div>
   
@endsection

