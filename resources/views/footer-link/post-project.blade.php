@extends('ui.maiong_ui.main')

@section('pageCss')
  <style>
    .box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    .mt1 {
      margin-top: 1em;
    }
    .mt2 {
      margin-top: 2em;
    }
    h3 {
      color: #16A085;
    }
  </style>
@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <h3>How To Post A Project</h3>
      
        <p>It is easy to post a project in Pick My Project with the following simple steps:</p>

       <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; If you are new to Pick My Project, then click on Sign Up.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Enter the required details in the form and click on Register.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; After registering your account successfully, click on the Post A Project.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Fill up all the required details for your project and click on Post Project.</li>
       </ul>

       <p>After posting a project, wait for freelancers to bid on your project and contact you. After selecting a freelancer for your project you can delete it from the platform. After 48 hours your project will be unpublished from the platform automatically.</p>

     
        </div>
      </div>

  

</div>
   
@endsection

