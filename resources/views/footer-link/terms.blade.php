@extends('ui.maiong_ui.main')

@section('pageCss')
  <style>
    .box {
      background-color: #fff;
      padding: 1em;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    .mt1 {
      margin-top: 1em;
    }
    .mt2 {
      margin-top: 2em;
    }
    .ml3 {
      margin-left: 3em;
    }
    h3 {
      color: #16A085;
    }
    h4 {
      color: #16A085;
    }
  </style>
@stop

@section('main_content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <h3>Welcome to Pick My Project</h3>
      
        <p>These terms and conditions outline the rules and regulations for the use of Pick My Project Website.</p>

        <p>By accessing this website we assume you accept these terms and conditions in full. Do not continue to use Pick My Project website if you do not accept all of the terms and conditions stated on this page.</p>

        <p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: “Client”, “You” and “Your” refers to you, the person accessing this website and accepting the Company’s terms and conditions. “The Company”, “Ourselves”, “We”, “Our” and “Us”, refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services/products, in accordance with and subject to, prevailing law of . Any use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>

        <h4>Cookies</h4>
        <p>We employ the use of cookies. By using Pick My Project you consent to the use of cookies in accordance with Pick My Project. Most of the modern day interactive web sites use cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate/advertising partners may also use cookies.</p>

        <h4>License</h4>
        <p>Unless otherwise stated, Pick My Project and/or it’s licensors own the intellectual property rights for all material on Pick My Project. All intellectual property rights are reserved. You may view and/or print pages from http://www.pickmyproject.com for your own personal use subject to restrictions set in these terms and conditions.</p>

        <p>You must not:</p>

       <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; Republish material from http://www.pickmyproject.com.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp;Sell, rent or sub-license material from http://www.pickmyproject.com.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Reproduce, duplicate or copy material from http://www.pickmyproject.com.</li>
         <li><i class="fa fa-arrow-right"></i> &nbsp; Redistribute content from Pick My Project (unless content is specifically made for redistribution).</li>
       </ul>

       <h4>Information You Provide to Us</h4>

       <p>Personal Information: In the course of using the Service (whether as a Client or Freelancer), we may require or otherwise collect information that identifies you as a specific individual and can be used to contact or identify you ("Personal Information"). “Personal information” is defined to include information that whether on its own or in combination with other information may be used to readily identify or contact you such as: name, address, email address, phone number etc.</p>

      <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; <b>Payment Information:</b> If you use the Service to make or receive payments, we may also collect certain payment information, such as payment card, PayPal or other financial account information, and billing address.</li>

         <li><i class="fa fa-arrow-right"></i> &nbsp; <b>Identity Verification:</b> We may collect Personal Information, such as your date of birth, or taxpayer identification number, to validate your identity or as may be required by law, such as completion of tax filings. We may request documents to verify this information, such as a copy of your government-issued identification or photo or a billing statement.</li>

         <li><i class="fa fa-arrow-right"></i> &nbsp; <b>Non-Identifying Information/User Names:</b> We also may collect other information, such as zip codes, demographic data, information regarding your use of the Service, and general project-related data ("Non-Identifying Information"). We may aggregate information collected from registered and non-registered users. We consider usernames to be Non-Identifying Information. Usernames are made public through the Service and are viewable by other Pick My Project Users.</li>

         <li><i class="fa fa-arrow-right"></i> &nbsp; <b>Combination of Personal and Non-Identifying Information:</b> Certain Non-Identifying Information would be considered a part of your Personal Information if it were combined with other identifiers in a way that enables you to be identified (for example, combining information with your name). But the same pieces of information are considered Non-Identifying Information when they are taken alone or combined only with other non-identifying information (for example, your viewing preferences). We may combine your Personal Information with Non-Identifying Information, but Pick My Project will treat the combined information as Personal Information.</li>

       </ul>

       <h4>Pricing</h4>
       <p>The pricing that is charged from the freelancers or service providers by Pick My Project may be increased or decreased at any period of time as per decision of Pick My Project.</p>

       <h4>User Comments</h4>
       <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; This Agreement shall begin on the date hereof.</li>

         <li><i class="fa fa-arrow-right"></i> &nbsp; Certain parts of this website offer the opportunity for users to post and exchange opinions, information, material and data ('Comments') in areas of the website. Pick My Project does not screen, edit, publish or review Comments prior to their appearance on the website and Comments do not reflect the views or opinions of Pick My Project, its agents or affiliates. Comments reflect the view and opinion of the person who posts such view or opinion. To the extent permitted by applicable laws Pick My Project shall not be responsible or liable for the Comments or for any loss cost, liability, damages or expenses caused and or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</li>

         <li><i class="fa fa-arrow-right"></i> &nbsp; Pick My Project reserves the right to monitor all Comments and to remove any Comments which it considers in its absolute discretion to be inappropriate, offensive or otherwise in breach of these Terms and Conditions.</li>

         <li><i class="fa fa-arrow-right"></i> &nbsp; You warrant and represent that:   

            <ul class="ml3">
              <li><i class="fa fa-caret-right"></i> &nbsp; You are entitled to post the Comments on our website and have all necessary licenses and consents to do so.</li>

              <li><i class="fa fa-caret-right"></i> &nbsp; The Comments do not infringe any intellectual property right, including without limitation copyright, patent or trademark, or other proprietary right of any third party.</li>

              <li><i class="fa fa-caret-right"></i> &nbsp; The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material or material which is an invasion of privacy.</li>

              <li><i class="fa fa-caret-right"></i> &nbsp; The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li>

              <li><i class="fa fa-caret-right"></i> &nbsp; You hereby grant to Pick My Project a non-exclusive royalty-free license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</li>
           </ul>

         </li>

       </ul>


       <h4>Hyperlinking to our Content</h4>
        <ul class="">
         <li><i class="fa fa-arrow-right"></i> &nbsp; The following organizations may link to our Web site without prior written approval: 
            <ul class="ml3">
              <li><i class="fa fa-caret-right"></i> &nbsp; Government agencies.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; Search engines.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; News organizations.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; Online directory distributors when they list us in the directory may link to our Web site in the same manner as they hyperlink to the Web sites of other listed businesses.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; Systemwide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</li>
            </ul>
         </li>

        <li><i class="fa fa-arrow-right"></i> &nbsp; These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party's site. </li>

        <li><i class="fa fa-arrow-right"></i> &nbsp; We may consider and approve in our sole discretion other link requests from the following types of organizations: 
          <ul class="ml3">
              <li><i class="fa fa-caret-right"></i> &nbsp; Commonly-known consumer and/or business information sources such as Chambers of Commerce, American Automobile Association, AARP and Consumers Union.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; Dot.com community sites.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; Associations or other groups representing charities, including charity giving sites.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; Online directory distributors.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; Internet portals.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; Accounting, law and consulting firms whose primary clients are businesses.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; Educational institutions and trade associations.</li>
            </ul>
        </li>

       </ul>

       <p>We will approve link requests from these organizations if we determine that:</p>
        <ul class="">
          <li><i class="fa fa-arrow-right"></i> &nbsp;  The link would not reflect unfavourably on us or our accredited businesses (for example, trade associations or other organizations representing inherently suspect types of business, such as work-at-home opportunities, shall not be allowed to link) </li>

          <li><i class="fa fa-arrow-right"></i> &nbsp;  The organization does not have an unsatisfactory record with us </li>

          <li><i class="fa fa-arrow-right"></i> &nbsp;  The benefit to us from the visibility associated with the hyperlink outweighs the absence of </li>

          <li><i class="fa fa-arrow-right"></i> &nbsp;  Where the link is in the context of general resource information or is otherwise consistent with editorial content in a newsletter or similar product furthering the mission of the organization.</li>

        </ul>

        <p>These organizations may link to our home page, to publications or to other Web site information so long as the link:</p>

       <ul class="">
          <li><i class="fa fa-arrow-right"></i> &nbsp;   is not in any way misleading </li>

          <li><i class="fa fa-arrow-right"></i> &nbsp; does not falsely imply sponsorship, endorsement or approval of the linking party and it products or services </li>

          <li><i class="fa fa-arrow-right"></i> &nbsp;  fits within the context of the linking party's site </li>

          <li>Approved organizations may hyperlink to our Web site as follows:
             <ul class="ml3">
              <li><i class="fa fa-caret-right"></i> &nbsp; By use of our corporate name; or.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; By use of the uniform resource locator (Web address) being linked to; or.</li>
              <li><i class="fa fa-caret-right"></i> &nbsp; By use of any other description of our Web site or material being linked to that makes sense within the context and format of content on the linking party's site.</li>
            </ul> 
          </li>

        </ul>
        <p>No use of Pick My Project’s logo or other artwork will be allowed for linking absent a trademark license agreement.</p>

        <h4>Iframes</h4>
        <p>Without prior approval and express written permission, you may not create frames around our Web pages or use other techniques that alter in any way the visual presentation or appearance of our Web site.</p>

        <h4>Reservation of Rights</h4>
        <p>We reserve the right at any time and in its sole discretion to request that you remove all links or any particular link to our Web site. You agree to immediately remove all links to our Web site upon such request. We also reserve the right to amend these terms and conditions and its linking policy at any time. By continuing to link to our Web site, you agree to be bound to and abide by these linking terms and conditions.</p>

        <h4>Removal of links from our website</h4>
        <p>If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.</p>

        <p>Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.</p>

        <h4>Content Liability</h4>
        <p>We shall have no responsibility or liability for any content appearing on your Web site. You agree to indemnify and defend us against all claims arising out of or based upon your Website. No link(s) may appear on any page on your Web site or within any context containing content or materials that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>

        <h4>Disclaimer</h4>
        <p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill). Nothing in this disclaimer will:</p>

        <ul>
          <li><i class="fa fa-arrow-right"></i> &nbsp;  limit or exclude our or your liability for death or personal injury resulting from negligence.</li>

          <li><i class="fa fa-arrow-right"></i> &nbsp;  limit or exclude our or your liability for fraud or fraudulent misrepresentation.</li>

          <li><i class="fa fa-arrow-right"></i> &nbsp;  limit any of our or your liabilities in any way that is not permitted under applicable law.</li>

          <li><i class="fa fa-arrow-right"></i> &nbsp;  exclude any of our or your liabilities that may not be excluded under applicable law.</li>

        </ul>

        <p><b>Pick My Project does not guarantee to give job to everyone who joins this platform. It is upto the freelancer or the service provider’s own talent to get a job or get hired by the clients.</b></p>

        <p>The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer or in relation to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.</p>

        <p>To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>

  
        </div>
      </div>

  

</div>
   
@endsection

