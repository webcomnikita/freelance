<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile_portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_profile_id', false, true);
            $table->string('title', 127);
            $table->string('slug', 255)->unique();
            $table->text('description');
            $table->string('image_path', 127);
            $table->string('web_link', 130);
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('user_profile_id')->references('id')->on('user_profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile_portfolios');
    }
}
