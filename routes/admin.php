<?php

Route::get('/home', function () {
    return view('admin.home');
})->name('home');


//MASTER CRUD

//category

Route::group(['prefix'=>'category'], function() {
    Route::get('/', [
        'as' => 'category.index',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CategoriesController@index'
    ]);

    Route::get('/create', [
        'as' => 'category.create',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CategoriesController@create'
    ]);

    Route::post('/save', [
        'as' => 'category.save',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CategoriesController@store'
    ]);

    Route::get('/{category_id}/edit', [
        'as' => 'category.edit',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CategoriesController@edit'
    ]);

    Route::post('/{category_id}/update', [
        'as' => 'category.update',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CategoriesController@update'
    ]);


    Route::get('/{category_id}/disable', [
        'as' => 'category.disable',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CategoriesController@disable'
    ]);
});


//sub-category
Route::group(['prefix'=>'sub-category'], function() {
    Route::get('/', [
        'as' => 'sub_category.index',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SubCategoriesController@index'
    ]);

    Route::get('/create', [
        'as' => 'sub_category.create',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SubCategoriesController@create'
    ]);

    Route::post('/save', [
        'as' => 'sub_category.save',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SubCategoriesController@store'
    ]);

    Route::get('/{sub_category_id}/edit', [
        'as' => 'sub_category.edit',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SubCategoriesController@edit'
    ]);

    Route::post('/{sub_category_id}/update', [
        'as' => 'sub_category.update',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SubCategoriesController@update'
    ]);


    Route::get('/{category_id}/disable', [
        'as' => 'sub_category.disable',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SubCategoriesController@disable'
    ]);
});

Route::group(['prefix'=>'user-details'], function() {

    Route::get('/', [
        'as' => 'user-details.index',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\UserDetailController@index'
    ]);

});

Route::group(['prefix'=>'seller-user-details'], function() {

    Route::get('/', [
        'as' => 'seller-user-details.index',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SellerUserDetailController@index'
    ]);

    Route::get('profile-detail/{slug}', [
        'as' => 'seller-user-detail.single',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SellerUserDetailController@show'
    ]);

    Route::post('/save', [
        'as' => 'seller-user-details.index.post',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SellerUserDetailController@store'
    ]);

    Route::post('/undoSave', [
        'as' => 'seller-user-details.index.undo.post',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\SellerUserDetailController@undoStore'
    ]);

});


// Coupons
Route::group(['prefix'=>'coupon'], function() {
    Route::get('/', [
        'as' => 'coupon.index',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CouponController@index'
    ]);

    Route::get('/create', [
        'as' => 'coupon.create',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CouponController@create'
    ]);

    Route::post('/save', [
        'as' => 'coupon.save',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CouponController@store'
    ]);

    Route::get('/{coupon_name}/edit', [
        'as' => 'coupon.edit',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CouponController@edit'
    ]);

    Route::patch('/{coupon_name}/update', [
        'as' => 'coupon.update',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CouponController@update'
    ]);


    Route::post('/{coupon_name}/disable', [
        'as' => 'coupon.disable',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\CouponController@disable'
    ]);
});

// disable time
Route::group(['prefix'=>'disable_time'], function() {
    Route::get('/', [
        'as' => 'disable_time.index',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\DisableTimeController@index'
    ]);

    Route::get('/edit/{id}', [
        'as' => 'disable_time.edit',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\DisableTimeController@edit'
    ]);

    Route::patch('/update/{id}', [
        'as' => 'disable_time.update',
        'middleware' => ['admin'],
        'uses' => 'Admin\Master\DisableTimeController@update'
    ]);
});

