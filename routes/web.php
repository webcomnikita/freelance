<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PublicController@viewHome')->name('home');

Route::group(['prefix' => 'categories'], function () {
  Route::get('/', 'PublicController@viewAllCategories')->name('public.view_categories');
  Route::get('/{category_slug}', 'PublicController@viewJobsByCategory')->name('public.view_jobs.categories');
});


Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::get('/logout', 'AdminAuth\LoginController@logout')->name('admin.logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
/*
Route::group(['prefix' => 'employee'], function () {
  Route::get('/login', 'EmployeeAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'EmployeeAuth\LoginController@login');
  Route::post('/logout', 'EmployeeAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'EmployeeAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'EmployeeAuth\RegisterController@register');

  Route::post('/password/email', 'EmployeeAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'EmployeeAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'EmployeeAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'EmployeeAuth\ResetPasswordController@showResetForm');
});
*/


Route::group(['prefix' => 'user'], function () {
  Route::get('/login', ['uses' => 'Auth\LoginController@showLoginForm', 'as' => 'login']);
  Route::post('/login', 'Auth\LoginController@login');
  Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
});

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/login/refresh', 'Auth\LoginController@refresh');

Route::group(['prefix' => 'user'], function () {
  Route::get('/login', 'Auth\LoginController@showLoginForm')->name('user.login');
  Route::post('/login', 'Auth\LoginController@login')->name('process.user.login');

  Route::get('/logout', 'Auth\LoginController@logout')->name('user.logout');

  Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('user.register');
  Route::post('/register', 'Auth\RegisterController@register')->name('save.user.register');
  Route::get('/confirm-email/{token}', 'Auth\RegisterController@confirmEmail')->name('user.email_confirm');

  Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('user.password.request');
  Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('user.password.email');
  Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('user.password.reset');
  Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
});


Route::group(['prefix' => 'projects'], function () {
  Route::get('/', [
      'as' => 'projects',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@view'
  ]);

  Route::get('/create', [
      'as' => 'projects.create',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@create'
  ]);

  Route::post('/save', [
      'as' => 'projects.store',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@store'
  ]);

  Route::post('/disable-status-save', [
      'as' => 'projects-disable-status.store',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@changeDisableProject'
  ]);

  Route::get('/{category_name}/{project_slug}', [
      'as' => 'project.view',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@show'
  ]);

  Route::get('/bidding-for/{slug}/project', [
      'as' => 'contact-me-project',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@contactMeForProjectDetails'
  ]);

  Route::post('/contact-me-for-project', [
        'as' => 'contact-me-for-project.post',
        'middleware' => ['auth'],
        'uses' => 'User\Projects\ProjectsController@contactMeProjectSend'
    ]);

  Route::get('/buy-premium', [
        'as' => 'user.bidding.buy-premium',
        'middleware' => ['auth'],
        'uses' => 'User\Projects\ProjectsController@buyPremium'
    ]);

  Route::post('/buy-premium-confirm', [
        'as' => 'user.bidding.buy-premium-confirm',
        'middleware' => ['auth'],
        'uses' => 'User\Projects\ProjectsController@buyPremiumConfirm'
    ]);

  Route::post('/buy-premium-coupon', [
        'as' => 'user.bidding.buy-premium-coupon',
        'middleware' => ['auth'],
        'uses' => 'User\Projects\ProjectsController@buyPremiumCoupon'
    ]);


  Route::post('/coupon-discounted-applied', [
        'as' => 'user.bidding.coupon-discounted-applied',
        'middleware' => ['auth'],
        'uses' => 'User\Projects\ProjectsController@paymentZeroPaid'
    ]);

  Route::post('/buy-premium-coupon-applied', [
        'as' => 'user.bidding.buy-premium-coupon-applied',
        'middleware' => ['auth'],
        'uses' => 'User\Projects\ProjectsController@paymentZeroPaid1'
    ]);

  Route::post('/buy-premium-init', [
        'as' => 'user.buy-premium-init.post',
        'middleware' => ['auth'],
        'uses' => 'User\Projects\ProjectsController@buyPremiumInit'
    ]);

    Route::post('/buy-premium-success', [
        'as' => 'user.buy-premium.success',
        'uses' => 'User\Projects\ProjectsController@buyPremiumSuccess'
    ]);

    Route::post('/buy-premium-fail', [
        'as' => 'user.buy-premium.fail',
        'uses' => 'User\Projects\ProjectsController@buyPremiumFail'
    ]);

 

});

Route::group(['prefix' => 'dashboard'], function () {
  Route::get('/', [
      'as' => 'dashboard',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@index'
  ]);
});

Route::group(['prefix' => 'browse-jobs'], function () {
  Route::get('/', [
      'as' => 'browse-jobs',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@browseJob'
  ]);

  Route::post('browse-job-by-filter', [
      'as' => 'browse-jobs-by-filter',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@browseJobFilter'
  ]);
});

Route::group(['prefix' => 'category'], function () {
  Route::get('/{slug}', [
      'as' => 'category',
      'middleware' => ['auth'],
      'uses' => 'User\Projects\ProjectsController@showCategoryFilter'
  ]);
});

Route::group(['prefix' => 'profile'], function () {
  Route::get('/', [
      'as' => 'profiles',
      'middleware' => ['auth'],
      'uses' => 'User\Profiles\ProfilesController@index'
  ]);

  Route::get('/create', [
      'as' => 'profile.create',
      'middleware' => ['auth'],
      'uses' => 'User\Profiles\ProfilesController@create'
  ]);

  Route::post('/save', [
      'as' => 'profile.store',
      'middleware' => ['auth'],
      'uses' => 'User\Profiles\ProfilesController@store'
  ]);

  Route::get('/view-details/{name}', [
      'as' => 'profile.view',
      'middleware' => ['auth'],
      'uses' => 'User\Profiles\ProfilesController@show'
  ]);

  Route::get('/search', [
      'as' => 'profile.search',
      'uses' => 'User\Profiles\ProfilesController@search'
  ]);

  Route::get('/my-profile', [
        'as' => 'user.my-profile',
        'middleware' => ['auth'],
        'uses' => 'User\Profiles\ProfilesController@myProfile'
    ]);

});


Route::get('/api/user-list1', [
  'as' => 'mail.user.list1',
  'uses' => 'User\Message\MessagesController@mailApiUsers1'
]);

Route::group(['prefix' => 'user'], function () {
  Route::group(['prefix' => 'message'], function () {
    Route::get('/compose', [
        'as' => 'user.message.compose',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@compose'
    ]);

    Route::post('/send', [
        'as' => 'user.message.send',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@send'
    ]);

    Route::get('/my-inbox', [
        'as' => 'user.message.my-inbox',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@myInbox'
    ]);

    Route::get('/my-inbox-reply/{id}', [
        'as' => 'user.message.my-inbox-reply',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@myInboxReply'
    ]);

    Route::get('/my-premium-inbox-reply/{id}', [
        'as' => 'user.message.my-inbox-reply-premium',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@myPremiumInboxReply'
    ]);

    Route::post('/my-inbox-reply-post', [
        'as' => 'user.message.my-inbox-reply.post',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@inboxReplySend'
    ]);

    



    

    // ---------------------------------------------------------------------------




    Route::get('/my-inbox-details/{id}', [
        'as' => 'user.message.my-inbox-details',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@fullInbox'
    ]);

    Route::get('/my-inquiry-details/{id}', [
        'as' => 'user.message.my-inquiry-details',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@fullInquiry'
    ]);

    Route::get('/my-sent-details/{id}', [
        'as' => 'user.message.my-sent-details',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@fullSent'
    ]);

    Route::get('/contact-me/{slug}', [
        'as' => 'user.message.contact-me',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@concactMe'
    ]);

    Route::post('/contact-me', [
        'as' => 'user.message.contact-me.sent',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@inquirySent'
    ]);

    Route::get('/order-now/{slug}', [
        'as' => 'user.order-now',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@orderNow'
    ]);

    Route::post('/order-now-init', [
        'as' => 'user.order-now-init.post',
        'middleware' => ['auth'],
        'uses' => 'User\Message\MessagesController@orderNowInit'
    ]);

    // Route::post('/test_route', [
    //     'as' => 'user.test_route.post',
    //     'uses' => 'User\Message\MessagesController@TestRedirect'
    // ]);


    Route::post('/order-now-success', [
        'as' => 'user.order-now.success',
        'uses' => 'User\Message\MessagesController@orderNowSuccess'
    ]);

    Route::post('/order-now-fail', [
        'as' => 'user.order-now.fail',
        'uses' => 'User\Message\MessagesController@orderNowFail'
    ]);

  });
});

Route::group(['prefix' => 'activity'], function () {
  Route::get('/my-activity', [
        'as' => 'user.my-activity',
        'middleware' => ['auth'],
        'uses' => 'User\Activity\ActivityController@index'
    ]);

  Route::get('/my-activity-seller/{id}', [
        'as' => 'user.my-activity-seller',
        'middleware' => ['auth'],
        'uses' => 'User\Activity\ActivityController@show'
    ]);

  Route::get('/my-activity-buyer/{id}', [
        'as' => 'user.my-activity-buyer',
        'middleware' => ['auth'],
        'uses' => 'User\Activity\ActivityController@buyerShow'
    ]);

  Route::post('/my-activity-seller', [
        'as' => 'user.my-activity-seller.post',
        'middleware' => ['auth'],
        'uses' => 'User\Activity\ActivityController@store'
    ]);

  Route::post('/my-activity-order-delivered', [
        'as' => 'user.my-activity-order-delivered.post',
        'middleware' => ['auth'],
        'uses' => 'User\Activity\ActivityController@deliverOrderBySeller'
    ]);

  Route::post('/my-activity-order-received-delivered', [
        'as' => 'user.my-activity-order-received-delivered.post',
        'middleware' => ['auth'],
        'uses' => 'User\Activity\ActivityController@deliverOrderByBuyer'
    ]);

});


// footer
Route::group(['prefix' => 'footer'], function () {
  Route::get('/about-company', [
        'as' => 'user.footer-about-company',
        'middleware' => ['auth'],
        'uses' => 'User\Footer\FooterController@aboutCompany'
    ]);

  Route::get('/for-business', [
        'as' => 'user.footer-for-business',
        'middleware' => ['auth'],
        'uses' => 'User\Footer\FooterController@business'
    ]);

  Route::get('/pmp-support', [
        'as' => 'user.footer-pmp-support',
        'middleware' => ['auth'],
        'uses' => 'User\Footer\FooterController@pmpSupport'
    ]);

  Route::get('/how-to-create-seller-profile', [
        'as' => 'user.footer-seller-profile',
        'middleware' => ['auth'],
        'uses' => 'User\Footer\FooterController@createSeller'
    ]);

  Route::get('/how-to-post-a-project', [
        'as' => 'user.footer-post-a-project',
        'middleware' => ['auth'],
        'uses' => 'User\Footer\FooterController@postProject'
    ]);

  Route::get('/posting-terms', [
        'as' => 'user.footer-posting-terms',
        'middleware' => ['auth'],
        'uses' => 'User\Footer\FooterController@postingTerm'
    ]);

  Route::get('/privacy-policy', [
        'as' => 'user.footer-policy',
        'middleware' => ['auth'],
        'uses' => 'User\Footer\FooterController@policy'
    ]);

  Route::get('/terms-and-condition', [
        'as' => 'user.footer-terms',
        'middleware' => ['auth'],
        'uses' => 'User\Footer\FooterController@term'
    ]);

  Route::get('/fees-and-charges', [
        'as' => 'user.footer-fees-and-charges',
        'middleware' => ['auth'],
        'uses' => 'User\Footer\FooterController@fees'
    ]);

});




Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

